//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class MetaPagination {
  /// Returns a new [MetaPagination] instance.
  MetaPagination({
    this.currentPage,
    this.perPage,
    this.lastPage,
    this.total,
  });

  int currentPage;

  int perPage;

  int lastPage;

  int total;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MetaPagination &&
     other.currentPage == currentPage &&
     other.perPage == perPage &&
     other.lastPage == lastPage &&
     other.total == total;

  @override
  int get hashCode =>
    (currentPage == null ? 0 : currentPage.hashCode) +
    (perPage == null ? 0 : perPage.hashCode) +
    (lastPage == null ? 0 : lastPage.hashCode) +
    (total == null ? 0 : total.hashCode);

  @override
  String toString() => 'MetaPagination[currentPage=$currentPage, perPage=$perPage, lastPage=$lastPage, total=$total]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (currentPage != null) {
      json[r'currentPage'] = currentPage;
    }
    if (perPage != null) {
      json[r'perPage'] = perPage;
    }
    if (lastPage != null) {
      json[r'lastPage'] = lastPage;
    }
    if (total != null) {
      json[r'total'] = total;
    }
    return json;
  }

  /// Returns a new [MetaPagination] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static MetaPagination fromJson(Map<String, dynamic> json) => json == null
    ? null
    : MetaPagination(
        currentPage: json[r'currentPage'],
        perPage: json[r'perPage'],
        lastPage: json[r'lastPage'],
        total: json[r'total'],
    );

  static List<MetaPagination> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <MetaPagination>[]
      : json.map((v) => MetaPagination.fromJson(v)).toList(growable: true == growable);

  static Map<String, MetaPagination> mapFromJson(Map<String, dynamic> json) {
    final map = <String, MetaPagination>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = MetaPagination.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of MetaPagination-objects as value to a dart map
  static Map<String, List<MetaPagination>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<MetaPagination>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = MetaPagination.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int currentPage,

        int perPage,

        int lastPage,

        int total,

    }) {
    return MetaPagination(
        currentPage: currentPage ?? this.currentPage,
        perPage: perPage ?? this.perPage,
        lastPage: lastPage ?? this.lastPage,
        total: total ?? this.total,
    );
    }


    }

