//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CouponShort {
  /// Returns a new [CouponShort] instance.
  CouponShort({
    this.id,
    this.name,
    this.finishedAt,
    this.finishedAtLabel,
    this.imageUrl,
    this.placeIds = const [],
    this.remind,
    this.productId,
    this.productExternalId,
    this.productName,
    this.price,
    this.productTypeId,
  });

  int id;

  /// Название купона
  String name;

  /// Дата окончания в формате 2020-11-08T00:00:00
  String finishedAt;

  /// Надпись даты окончания в формате до 25 января 2020
  String finishedAtLabel;

  /// Ссылка на файл изображения купона
  String imageUrl;

  /// Id торговых точек
  List<String> placeIds;

  /// Признак уведомлять ли о купоне при приближении к АЗС
  bool remind;

  /// Id товара
  String productId;

  /// Номенклатурный номер товара
  String productExternalId;

  /// Название товара
  String productName;

  /// Цена товара по купону
  String price;

  /// Id типа товара. Если равно 1, то это цена любимого топлива клиента
  int productTypeId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CouponShort &&
     other.id == id &&
     other.name == name &&
     other.finishedAt == finishedAt &&
     other.finishedAtLabel == finishedAtLabel &&
     other.imageUrl == imageUrl &&
     other.placeIds == placeIds &&
     other.remind == remind &&
     other.productId == productId &&
     other.productExternalId == productExternalId &&
     other.productName == productName &&
     other.price == price &&
     other.productTypeId == productTypeId;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (finishedAt == null ? 0 : finishedAt.hashCode) +
    (finishedAtLabel == null ? 0 : finishedAtLabel.hashCode) +
    (imageUrl == null ? 0 : imageUrl.hashCode) +
    (placeIds == null ? 0 : placeIds.hashCode) +
    (remind == null ? 0 : remind.hashCode) +
    (productId == null ? 0 : productId.hashCode) +
    (productExternalId == null ? 0 : productExternalId.hashCode) +
    (productName == null ? 0 : productName.hashCode) +
    (price == null ? 0 : price.hashCode) +
    (productTypeId == null ? 0 : productTypeId.hashCode);

  @override
  String toString() => 'CouponShort[id=$id, name=$name, finishedAt=$finishedAt, finishedAtLabel=$finishedAtLabel, imageUrl=$imageUrl, placeIds=$placeIds, remind=$remind, productId=$productId, productExternalId=$productExternalId, productName=$productName, price=$price, productTypeId=$productTypeId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (finishedAt != null) {
      json[r'finishedAt'] = finishedAt;
    }
    if (finishedAtLabel != null) {
      json[r'finishedAtLabel'] = finishedAtLabel;
    }
    if (imageUrl != null) {
      json[r'imageUrl'] = imageUrl;
    }
    if (placeIds != null) {
      json[r'placeIds'] = placeIds;
    }
    if (remind != null) {
      json[r'remind'] = remind;
    }
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (productExternalId != null) {
      json[r'productExternalId'] = productExternalId;
    }
    if (productName != null) {
      json[r'productName'] = productName;
    }
    if (price != null) {
      json[r'price'] = price;
    }
    if (productTypeId != null) {
      json[r'productTypeId'] = productTypeId;
    }
    return json;
  }

  /// Returns a new [CouponShort] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CouponShort fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CouponShort(
        id: json[r'id'],
        name: json[r'name'],
        finishedAt: json[r'finishedAt'],
        finishedAtLabel: json[r'finishedAtLabel'],
        imageUrl: json[r'imageUrl'],
        placeIds: json[r'placeIds'] == null
          ? null
          : (json[r'placeIds'] as List).cast<String>(),
        remind: json[r'remind'],
        productId: json[r'productId'],
        productExternalId: json[r'productExternalId'],
        productName: json[r'productName'],
        price: json[r'price'],
        productTypeId: json[r'productTypeId'],
    );

  static List<CouponShort> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CouponShort>[]
      : json.map((v) => CouponShort.fromJson(v)).toList(growable: true == growable);

  static Map<String, CouponShort> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CouponShort>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CouponShort.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CouponShort-objects as value to a dart map
  static Map<String, List<CouponShort>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CouponShort>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CouponShort.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String name,

        String finishedAt,

        String finishedAtLabel,

        String imageUrl,

        List<String> placeIds,

        bool remind,

        String productId,

        String productExternalId,

        String productName,

        String price,

        int productTypeId,

    }) {
    return CouponShort(
        id: id ?? this.id,
        name: name ?? this.name,
        finishedAt: finishedAt ?? this.finishedAt,
        finishedAtLabel: finishedAtLabel ?? this.finishedAtLabel,
        imageUrl: imageUrl ?? this.imageUrl,
        placeIds: placeIds ?? this.placeIds,
        remind: remind ?? this.remind,
        productId: productId ?? this.productId,
        productExternalId: productExternalId ?? this.productExternalId,
        productName: productName ?? this.productName,
        price: price ?? this.price,
        productTypeId: productTypeId ?? this.productTypeId,
    );
    }


    }

