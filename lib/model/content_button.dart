//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ContentButton {
  /// Returns a new [ContentButton] instance.
  ContentButton({
    this.action,
    this.label,
  });

  /// Действие по нажатию кнопки
  ContentButtonActionEnum action;

  /// Надпись на кнопке
  String label;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ContentButton &&
     other.action == action &&
     other.label == label;

  @override
  int get hashCode =>
    (action == null ? 0 : action.hashCode) +
    (label == null ? 0 : label.hashCode);

  @override
  String toString() => 'ContentButton[action=$action, label=$label]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (action != null) {
      json[r'action'] = action;
    }
    if (label != null) {
      json[r'label'] = label;
    }
    return json;
  }

  /// Returns a new [ContentButton] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ContentButton fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ContentButton(
        action: ContentButtonActionEnum.fromJson(json[r'action']),
        label: json[r'label'],
    );

  static List<ContentButton> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ContentButton>[]
      : json.map((v) => ContentButton.fromJson(v)).toList(growable: true == growable);

  static Map<String, ContentButton> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ContentButton>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ContentButton.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ContentButton-objects as value to a dart map
  static Map<String, List<ContentButton>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ContentButton>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ContentButton.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        ContentButtonActionEnum action,

        String label,

    }) {
    return ContentButton(
        action: action ?? this.action,
        label: label ?? this.label,
    );
    }


    }

/// Действие по нажатию кнопки
class ContentButtonActionEnum {
  /// Instantiate a new enum with the provided [value].
  const ContentButtonActionEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  bool operator ==(Object other) => identical(this, other) ||
      other is ContentButtonActionEnum && other.value == value;

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() => value;

  String toJson() => value;

  static const next = ContentButtonActionEnum._(r'next');
  static const enableNotifications = ContentButtonActionEnum._(r'enable-notifications');
  static const enableGeo = ContentButtonActionEnum._(r'enable-geo');
  static const auth = ContentButtonActionEnum._(r'auth');
  static const withoutAuth = ContentButtonActionEnum._(r'without-auth');

  /// List of all possible values in this [enum][ContentButtonActionEnum].
  static const values = <ContentButtonActionEnum>[
    next,
    enableNotifications,
    enableGeo,
    auth,
    withoutAuth,
  ];

  static ContentButtonActionEnum fromJson(dynamic value) =>
    ContentButtonActionEnumTypeTransformer().decode(value);

  static List<ContentButtonActionEnum> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ContentButtonActionEnum>[]
      : json
          .map((value) => ContentButtonActionEnum.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [ContentButtonActionEnum] to String,
/// and [decode] dynamic data back to [ContentButtonActionEnum].
class ContentButtonActionEnumTypeTransformer {
  const ContentButtonActionEnumTypeTransformer._();

  factory ContentButtonActionEnumTypeTransformer() => _instance ??= ContentButtonActionEnumTypeTransformer._();

  String encode(ContentButtonActionEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a ContentButtonActionEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  ContentButtonActionEnum decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'next': return ContentButtonActionEnum.next;
      case r'enable-notifications': return ContentButtonActionEnum.enableNotifications;
      case r'enable-geo': return ContentButtonActionEnum.enableGeo;
      case r'auth': return ContentButtonActionEnum.auth;
      case r'without-auth': return ContentButtonActionEnum.withoutAuth;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [ContentButtonActionEnumTypeTransformer] instance.
  static ContentButtonActionEnumTypeTransformer _instance;
}

