//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ContentArticle {
  /// Returns a new [ContentArticle] instance.
  ContentArticle({
    this.id,
    this.code,
    this.title,
    this.description,
    this.imageUrl,
    this.sort,
    this.buttons = const [],
    this.contentButtons = const [],
  });

  /// Id статьи
  int id;

  /// Код статьи
  String code;

  /// Заголовок статьи
  String title;

  /// Текст статьи
  String description;

  /// Ссылка на изображение статьи
  String imageUrl;

  /// Индекс сортировки
  int sort;

  List<ContentArticleButtonsEnum> buttons;

  List<ContentButton> contentButtons;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ContentArticle &&
     other.id == id &&
     other.code == code &&
     other.title == title &&
     other.description == description &&
     other.imageUrl == imageUrl &&
     other.sort == sort &&
     other.buttons == buttons &&
     other.contentButtons == contentButtons;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (code == null ? 0 : code.hashCode) +
    (title == null ? 0 : title.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (imageUrl == null ? 0 : imageUrl.hashCode) +
    (sort == null ? 0 : sort.hashCode) +
    (buttons == null ? 0 : buttons.hashCode) +
    (contentButtons == null ? 0 : contentButtons.hashCode);

  @override
  String toString() => 'ContentArticle[id=$id, code=$code, title=$title, description=$description, imageUrl=$imageUrl, sort=$sort, buttons=$buttons, contentButtons=$contentButtons]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (code != null) {
      json[r'code'] = code;
    }
    if (title != null) {
      json[r'title'] = title;
    }
    if (description != null) {
      json[r'description'] = description;
    }
    if (imageUrl != null) {
      json[r'imageUrl'] = imageUrl;
    }
    if (sort != null) {
      json[r'sort'] = sort;
    }
    if (buttons != null) {
      json[r'buttons'] = buttons;
    }
    if (contentButtons != null) {
      json[r'contentButtons'] = contentButtons;
    }
    return json;
  }

  /// Returns a new [ContentArticle] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ContentArticle fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ContentArticle(
        id: json[r'id'],
        code: json[r'code'],
        title: json[r'title'],
        description: json[r'description'],
        imageUrl: json[r'imageUrl'],
        sort: json[r'sort'],
        buttons: ContentArticleButtonsEnum.listFromJson(json[r'buttons']),
        contentButtons: ContentButton.listFromJson(json[r'contentButtons']),
    );

  static List<ContentArticle> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ContentArticle>[]
      : json.map((v) => ContentArticle.fromJson(v)).toList(growable: true == growable);

  static Map<String, ContentArticle> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ContentArticle>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ContentArticle.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ContentArticle-objects as value to a dart map
  static Map<String, List<ContentArticle>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ContentArticle>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ContentArticle.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String code,

        String title,

        String description,

        String imageUrl,

        int sort,

        List<ContentArticleButtonsEnum> buttons,

        List<ContentButton> contentButtons,

    }) {
    return ContentArticle(
        id: id ?? this.id,
        code: code ?? this.code,
        title: title ?? this.title,
        description: description ?? this.description,
        imageUrl: imageUrl ?? this.imageUrl,
        sort: sort ?? this.sort,
        buttons: buttons ?? this.buttons,
        contentButtons: contentButtons ?? this.contentButtons,
    );
    }


    }


class ContentArticleButtonsEnum {
  /// Instantiate a new enum with the provided [value].
  const ContentArticleButtonsEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  bool operator ==(Object other) => identical(this, other) ||
      other is ContentArticleButtonsEnum && other.value == value;

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() => value;

  String toJson() => value;

  static const next = ContentArticleButtonsEnum._(r'next');
  static const enableNotifications = ContentArticleButtonsEnum._(r'enable-notifications');
  static const enableGeo = ContentArticleButtonsEnum._(r'enable-geo');
  static const auth = ContentArticleButtonsEnum._(r'auth');
  static const withoutAuth = ContentArticleButtonsEnum._(r'without-auth');

  /// List of all possible values in this [enum][ContentArticleButtonsEnum].
  static const values = <ContentArticleButtonsEnum>[
    next,
    enableNotifications,
    enableGeo,
    auth,
    withoutAuth,
  ];

  static ContentArticleButtonsEnum fromJson(dynamic value) =>
    ContentArticleButtonsEnumTypeTransformer().decode(value);

  static List<ContentArticleButtonsEnum> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ContentArticleButtonsEnum>[]
      : json
          .map((value) => ContentArticleButtonsEnum.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [ContentArticleButtonsEnum] to String,
/// and [decode] dynamic data back to [ContentArticleButtonsEnum].
class ContentArticleButtonsEnumTypeTransformer {
  const ContentArticleButtonsEnumTypeTransformer._();

  factory ContentArticleButtonsEnumTypeTransformer() => _instance ??= ContentArticleButtonsEnumTypeTransformer._();

  String encode(ContentArticleButtonsEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a ContentArticleButtonsEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  ContentArticleButtonsEnum decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'next': return ContentArticleButtonsEnum.next;
      case r'enable-notifications': return ContentArticleButtonsEnum.enableNotifications;
      case r'enable-geo': return ContentArticleButtonsEnum.enableGeo;
      case r'auth': return ContentArticleButtonsEnum.auth;
      case r'without-auth': return ContentArticleButtonsEnum.withoutAuth;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [ContentArticleButtonsEnumTypeTransformer] instance.
  static ContentArticleButtonsEnumTypeTransformer _instance;
}

