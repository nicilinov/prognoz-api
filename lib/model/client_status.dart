//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ClientStatus {
  /// Returns a new [ClientStatus] instance.
  ClientStatus({
    this.id,
    this.name,
    this.remindedAt,
    this.hasCouponReminds,
  });

  /// Код статуса клиента
  ClientStatusIdEnum id;

  /// Название статуса клиента
  String name;

  /// Дата и время последнего уведомления о купонах при приближении к АЗС
  String remindedAt;

  /// Признак, что у клиента есть актуальные купоны с уведомлениями
  bool hasCouponReminds;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ClientStatus &&
     other.id == id &&
     other.name == name &&
     other.remindedAt == remindedAt &&
     other.hasCouponReminds == hasCouponReminds;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (remindedAt == null ? 0 : remindedAt.hashCode) +
    (hasCouponReminds == null ? 0 : hasCouponReminds.hashCode);

  @override
  String toString() => 'ClientStatus[id=$id, name=$name, remindedAt=$remindedAt, hasCouponReminds=$hasCouponReminds]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (remindedAt != null) {
      json[r'remindedAt'] = remindedAt;
    }
    if (hasCouponReminds != null) {
      json[r'hasCouponReminds'] = hasCouponReminds;
    }
    return json;
  }

  /// Returns a new [ClientStatus] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ClientStatus fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ClientStatus(
        id: ClientStatusIdEnum.fromJson(json[r'id']),
        name: json[r'name'],
        remindedAt: json[r'remindedAt'],
        hasCouponReminds: json[r'hasCouponReminds'],
    );

  static List<ClientStatus> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ClientStatus>[]
      : json.map((v) => ClientStatus.fromJson(v)).toList(growable: true == growable);

  static Map<String, ClientStatus> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ClientStatus>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ClientStatus.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ClientStatus-objects as value to a dart map
  static Map<String, List<ClientStatus>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ClientStatus>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ClientStatus.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        ClientStatusIdEnum id,

        String name,

        String remindedAt,

        bool hasCouponReminds,

    }) {
    return ClientStatus(
        id: id ?? this.id,
        name: name ?? this.name,
        remindedAt: remindedAt ?? this.remindedAt,
        hasCouponReminds: hasCouponReminds ?? this.hasCouponReminds,
    );
    }


    }

/// Код статуса клиента
class ClientStatusIdEnum {
  /// Instantiate a new enum with the provided [value].
  const ClientStatusIdEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  bool operator ==(Object other) => identical(this, other) ||
      other is ClientStatusIdEnum && other.value == value;

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() => value;

  String toJson() => value;

  static const active = ClientStatusIdEnum._(r'active');
  static const inactive = ClientStatusIdEnum._(r'inactive');
  static const blocked = ClientStatusIdEnum._(r'blocked');

  /// List of all possible values in this [enum][ClientStatusIdEnum].
  static const values = <ClientStatusIdEnum>[
    active,
    inactive,
    blocked,
  ];

  static ClientStatusIdEnum fromJson(dynamic value) =>
    ClientStatusIdEnumTypeTransformer().decode(value);

  static List<ClientStatusIdEnum> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ClientStatusIdEnum>[]
      : json
          .map((value) => ClientStatusIdEnum.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [ClientStatusIdEnum] to String,
/// and [decode] dynamic data back to [ClientStatusIdEnum].
class ClientStatusIdEnumTypeTransformer {
  const ClientStatusIdEnumTypeTransformer._();

  factory ClientStatusIdEnumTypeTransformer() => _instance ??= ClientStatusIdEnumTypeTransformer._();

  String encode(ClientStatusIdEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a ClientStatusIdEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  ClientStatusIdEnum decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'active': return ClientStatusIdEnum.active;
      case r'inactive': return ClientStatusIdEnum.inactive;
      case r'blocked': return ClientStatusIdEnum.blocked;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [ClientStatusIdEnumTypeTransformer] instance.
  static ClientStatusIdEnumTypeTransformer _instance;
}

