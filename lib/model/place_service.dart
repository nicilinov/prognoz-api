//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class PlaceService {
  /// Returns a new [PlaceService] instance.
  PlaceService({
    this.id,
    this.name,
    this.iconUrl,
  });

  /// Id услуги
  int id;

  /// Название услуги
  String name;

  /// Ссылка на файл иконки услуги
  String iconUrl;

  @override
  bool operator ==(Object other) => identical(this, other) || other is PlaceService &&
     other.id == id &&
     other.name == name &&
     other.iconUrl == iconUrl;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (iconUrl == null ? 0 : iconUrl.hashCode);

  @override
  String toString() => 'PlaceService[id=$id, name=$name, iconUrl=$iconUrl]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (iconUrl != null) {
      json[r'iconUrl'] = iconUrl;
    }
    return json;
  }

  /// Returns a new [PlaceService] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static PlaceService fromJson(Map<String, dynamic> json) => json == null
    ? null
    : PlaceService(
        id: json[r'id'],
        name: json[r'name'],
        iconUrl: json[r'iconUrl'],
    );

  static List<PlaceService> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <PlaceService>[]
      : json.map((v) => PlaceService.fromJson(v)).toList(growable: true == growable);

  static Map<String, PlaceService> mapFromJson(Map<String, dynamic> json) {
    final map = <String, PlaceService>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = PlaceService.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of PlaceService-objects as value to a dart map
  static Map<String, List<PlaceService>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<PlaceService>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = PlaceService.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String name,

        String iconUrl,

    }) {
    return PlaceService(
        id: id ?? this.id,
        name: name ?? this.name,
        iconUrl: iconUrl ?? this.iconUrl,
    );
    }


    }

