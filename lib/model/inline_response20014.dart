//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse20014 {
  /// Returns a new [InlineResponse20014] instance.
  InlineResponse20014({
    this.data = const [],
  });

  List<MessageSubject> data;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse20014 &&
     other.data == data;

  @override
  int get hashCode =>
    (data == null ? 0 : data.hashCode);

  @override
  String toString() => 'InlineResponse20014[data=$data]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (data != null) {
      json[r'data'] = data;
    }
    return json;
  }

  /// Returns a new [InlineResponse20014] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse20014 fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse20014(
        data: MessageSubject.listFromJson(json[r'data']),
    );

  static List<InlineResponse20014> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse20014>[]
      : json.map((v) => InlineResponse20014.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse20014> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse20014>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse20014.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse20014-objects as value to a dart map
  static Map<String, List<InlineResponse20014>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse20014>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse20014.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        List<MessageSubject> data,

    }) {
    return InlineResponse20014(
        data: data ?? this.data,
    );
    }


    }

