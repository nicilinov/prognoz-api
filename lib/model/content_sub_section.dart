//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ContentSubSection {
  /// Returns a new [ContentSubSection] instance.
  ContentSubSection({
    this.id,
    this.code,
    this.title,
    this.description,
    this.imageUrl,
    this.sort,
    this.buttons = const [],
    this.contentButtons = const [],
    this.children = const [],
  });

  /// Id подраздела
  int id;

  /// Код подраздела
  String code;

  /// Заголовок статьи
  String title;

  /// Текст статьи
  String description;

  /// Ссылка на изображение статьи
  String imageUrl;

  /// Индекс сортировки
  int sort;

  List<ContentSubSectionButtonsEnum> buttons;

  List<ContentButton> contentButtons;

  List<ContentArticle> children;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ContentSubSection &&
     other.id == id &&
     other.code == code &&
     other.title == title &&
     other.description == description &&
     other.imageUrl == imageUrl &&
     other.sort == sort &&
     other.buttons == buttons &&
     other.contentButtons == contentButtons &&
     other.children == children;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (code == null ? 0 : code.hashCode) +
    (title == null ? 0 : title.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (imageUrl == null ? 0 : imageUrl.hashCode) +
    (sort == null ? 0 : sort.hashCode) +
    (buttons == null ? 0 : buttons.hashCode) +
    (contentButtons == null ? 0 : contentButtons.hashCode) +
    (children == null ? 0 : children.hashCode);

  @override
  String toString() => 'ContentSubSection[id=$id, code=$code, title=$title, description=$description, imageUrl=$imageUrl, sort=$sort, buttons=$buttons, contentButtons=$contentButtons, children=$children]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (code != null) {
      json[r'code'] = code;
    }
    if (title != null) {
      json[r'title'] = title;
    }
    if (description != null) {
      json[r'description'] = description;
    }
    if (imageUrl != null) {
      json[r'imageUrl'] = imageUrl;
    }
    if (sort != null) {
      json[r'sort'] = sort;
    }
    if (buttons != null) {
      json[r'buttons'] = buttons;
    }
    if (contentButtons != null) {
      json[r'contentButtons'] = contentButtons;
    }
    if (children != null) {
      json[r'children'] = children;
    }
    return json;
  }

  /// Returns a new [ContentSubSection] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ContentSubSection fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ContentSubSection(
        id: json[r'id'],
        code: json[r'code'],
        title: json[r'title'],
        description: json[r'description'],
        imageUrl: json[r'imageUrl'],
        sort: json[r'sort'],
        buttons: ContentSubSectionButtonsEnum.listFromJson(json[r'buttons']),
        contentButtons: ContentButton.listFromJson(json[r'contentButtons']),
        children: ContentArticle.listFromJson(json[r'children']),
    );

  static List<ContentSubSection> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ContentSubSection>[]
      : json.map((v) => ContentSubSection.fromJson(v)).toList(growable: true == growable);

  static Map<String, ContentSubSection> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ContentSubSection>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ContentSubSection.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ContentSubSection-objects as value to a dart map
  static Map<String, List<ContentSubSection>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ContentSubSection>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ContentSubSection.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String code,

        String title,

        String description,

        String imageUrl,

        int sort,

        List<ContentSubSectionButtonsEnum> buttons,

        List<ContentButton> contentButtons,

        List<ContentArticle> children,

    }) {
    return ContentSubSection(
        id: id ?? this.id,
        code: code ?? this.code,
        title: title ?? this.title,
        description: description ?? this.description,
        imageUrl: imageUrl ?? this.imageUrl,
        sort: sort ?? this.sort,
        buttons: buttons ?? this.buttons,
        contentButtons: contentButtons ?? this.contentButtons,
        children: children ?? this.children,
    );
    }


    }


class ContentSubSectionButtonsEnum {
  /// Instantiate a new enum with the provided [value].
  const ContentSubSectionButtonsEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  bool operator ==(Object other) => identical(this, other) ||
      other is ContentSubSectionButtonsEnum && other.value == value;

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() => value;

  String toJson() => value;

  static const next = ContentSubSectionButtonsEnum._(r'next');
  static const enableNotifications = ContentSubSectionButtonsEnum._(r'enable-notifications');
  static const enableGeo = ContentSubSectionButtonsEnum._(r'enable-geo');
  static const auth = ContentSubSectionButtonsEnum._(r'auth');
  static const withoutAuth = ContentSubSectionButtonsEnum._(r'without-auth');

  /// List of all possible values in this [enum][ContentSubSectionButtonsEnum].
  static const values = <ContentSubSectionButtonsEnum>[
    next,
    enableNotifications,
    enableGeo,
    auth,
    withoutAuth,
  ];

  static ContentSubSectionButtonsEnum fromJson(dynamic value) =>
    ContentSubSectionButtonsEnumTypeTransformer().decode(value);

  static List<ContentSubSectionButtonsEnum> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ContentSubSectionButtonsEnum>[]
      : json
          .map((value) => ContentSubSectionButtonsEnum.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [ContentSubSectionButtonsEnum] to String,
/// and [decode] dynamic data back to [ContentSubSectionButtonsEnum].
class ContentSubSectionButtonsEnumTypeTransformer {
  const ContentSubSectionButtonsEnumTypeTransformer._();

  factory ContentSubSectionButtonsEnumTypeTransformer() => _instance ??= ContentSubSectionButtonsEnumTypeTransformer._();

  String encode(ContentSubSectionButtonsEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a ContentSubSectionButtonsEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  ContentSubSectionButtonsEnum decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'next': return ContentSubSectionButtonsEnum.next;
      case r'enable-notifications': return ContentSubSectionButtonsEnum.enableNotifications;
      case r'enable-geo': return ContentSubSectionButtonsEnum.enableGeo;
      case r'auth': return ContentSubSectionButtonsEnum.auth;
      case r'without-auth': return ContentSubSectionButtonsEnum.withoutAuth;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [ContentSubSectionButtonsEnumTypeTransformer] instance.
  static ContentSubSectionButtonsEnumTypeTransformer _instance;
}

