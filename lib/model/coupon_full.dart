//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CouponFull {
  /// Returns a new [CouponFull] instance.
  CouponFull({
    this.id,
    this.name,
    this.description,
    this.finishedAt,
    this.finishedAtLabel,
    this.placeIds = const [],
    this.imageUrl,
    this.galleryUrls = const [],
    this.number,
    this.barcode,
    this.remind,
  });

  int id;

  /// Название купона
  String name;

  /// Описание купона
  String description;

  /// Дата окончания в формате 2020-11-08T00:00:00
  String finishedAt;

  /// Надпись даты окончания в формате до 25 января 2020
  String finishedAtLabel;

  /// Id торговых точек
  List<String> placeIds;

  /// Ссылка на файл изображения купона
  String imageUrl;

  List<String> galleryUrls;

  /// код купона из 10 цифр
  String number;

  /// Шрих-код купона в формате изображения в base64
  String barcode;

  /// Признак уведомлять ли о купоне при приближении к АЗС
  bool remind;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CouponFull &&
     other.id == id &&
     other.name == name &&
     other.description == description &&
     other.finishedAt == finishedAt &&
     other.finishedAtLabel == finishedAtLabel &&
     other.placeIds == placeIds &&
     other.imageUrl == imageUrl &&
     other.galleryUrls == galleryUrls &&
     other.number == number &&
     other.barcode == barcode &&
     other.remind == remind;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (finishedAt == null ? 0 : finishedAt.hashCode) +
    (finishedAtLabel == null ? 0 : finishedAtLabel.hashCode) +
    (placeIds == null ? 0 : placeIds.hashCode) +
    (imageUrl == null ? 0 : imageUrl.hashCode) +
    (galleryUrls == null ? 0 : galleryUrls.hashCode) +
    (number == null ? 0 : number.hashCode) +
    (barcode == null ? 0 : barcode.hashCode) +
    (remind == null ? 0 : remind.hashCode);

  @override
  String toString() => 'CouponFull[id=$id, name=$name, description=$description, finishedAt=$finishedAt, finishedAtLabel=$finishedAtLabel, placeIds=$placeIds, imageUrl=$imageUrl, galleryUrls=$galleryUrls, number=$number, barcode=$barcode, remind=$remind]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (description != null) {
      json[r'description'] = description;
    }
    if (finishedAt != null) {
      json[r'finishedAt'] = finishedAt;
    }
    if (finishedAtLabel != null) {
      json[r'finishedAtLabel'] = finishedAtLabel;
    }
    if (placeIds != null) {
      json[r'placeIds'] = placeIds;
    }
    if (imageUrl != null) {
      json[r'imageUrl'] = imageUrl;
    }
    if (galleryUrls != null) {
      json[r'galleryUrls'] = galleryUrls;
    }
    if (number != null) {
      json[r'number'] = number;
    }
    if (barcode != null) {
      json[r'barcode'] = barcode;
    }
    if (remind != null) {
      json[r'remind'] = remind;
    }
    return json;
  }

  /// Returns a new [CouponFull] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CouponFull fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CouponFull(
        id: json[r'id'],
        name: json[r'name'],
        description: json[r'description'],
        finishedAt: json[r'finishedAt'],
        finishedAtLabel: json[r'finishedAtLabel'],
        placeIds: json[r'placeIds'] == null
          ? null
          : (json[r'placeIds'] as List).cast<String>(),
        imageUrl: json[r'imageUrl'],
        galleryUrls: json[r'galleryUrls'] == null
          ? null
          : (json[r'galleryUrls'] as List).cast<String>(),
        number: json[r'number'],
        barcode: json[r'barcode'],
        remind: json[r'remind'],
    );

  static List<CouponFull> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CouponFull>[]
      : json.map((v) => CouponFull.fromJson(v)).toList(growable: true == growable);

  static Map<String, CouponFull> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CouponFull>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CouponFull.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CouponFull-objects as value to a dart map
  static Map<String, List<CouponFull>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CouponFull>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CouponFull.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String name,

        String description,

        String finishedAt,

        String finishedAtLabel,

        List<String> placeIds,

        String imageUrl,

        List<String> galleryUrls,

        String number,

        String barcode,

        bool remind,

    }) {
    return CouponFull(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        finishedAt: finishedAt ?? this.finishedAt,
        finishedAtLabel: finishedAtLabel ?? this.finishedAtLabel,
        placeIds: placeIds ?? this.placeIds,
        imageUrl: imageUrl ?? this.imageUrl,
        galleryUrls: galleryUrls ?? this.galleryUrls,
        number: number ?? this.number,
        barcode: barcode ?? this.barcode,
        remind: remind ?? this.remind,
    );
    }


    }

