//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse2006Data {
  /// Returns a new [InlineResponse2006Data] instance.
  InlineResponse2006Data({
    this.result,
  });

  bool result;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse2006Data &&
     other.result == result;

  @override
  int get hashCode =>
    (result == null ? 0 : result.hashCode);

  @override
  String toString() => 'InlineResponse2006Data[result=$result]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (result != null) {
      json[r'result'] = result;
    }
    return json;
  }

  /// Returns a new [InlineResponse2006Data] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse2006Data fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse2006Data(
        result: json[r'result'],
    );

  static List<InlineResponse2006Data> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse2006Data>[]
      : json.map((v) => InlineResponse2006Data.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse2006Data> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse2006Data>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse2006Data.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse2006Data-objects as value to a dart map
  static Map<String, List<InlineResponse2006Data>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse2006Data>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse2006Data.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        bool result,

    }) {
    return InlineResponse2006Data(
        result: result ?? this.result,
    );
    }


    }

