//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ContentSection {
  /// Returns a new [ContentSection] instance.
  ContentSection({
    this.id,
    this.code,
    this.title,
    this.description,
    this.imageUrl,
    this.sort,
    this.children = const [],
  });

  /// Id раздела
  int id;

  /// Код раздела
  String code;

  /// Заголовок статьи
  String title;

  /// Текст статьи
  String description;

  /// Ссылка на изображение статьи
  String imageUrl;

  /// Индекс сортировки
  int sort;

  List<ContentSubSection> children;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ContentSection &&
     other.id == id &&
     other.code == code &&
     other.title == title &&
     other.description == description &&
     other.imageUrl == imageUrl &&
     other.sort == sort &&
     other.children == children;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (code == null ? 0 : code.hashCode) +
    (title == null ? 0 : title.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (imageUrl == null ? 0 : imageUrl.hashCode) +
    (sort == null ? 0 : sort.hashCode) +
    (children == null ? 0 : children.hashCode);

  @override
  String toString() => 'ContentSection[id=$id, code=$code, title=$title, description=$description, imageUrl=$imageUrl, sort=$sort, children=$children]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (code != null) {
      json[r'code'] = code;
    }
    if (title != null) {
      json[r'title'] = title;
    }
    if (description != null) {
      json[r'description'] = description;
    }
    if (imageUrl != null) {
      json[r'imageUrl'] = imageUrl;
    }
    if (sort != null) {
      json[r'sort'] = sort;
    }
    if (children != null) {
      json[r'children'] = children;
    }
    return json;
  }

  /// Returns a new [ContentSection] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ContentSection fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ContentSection(
        id: json[r'id'],
        code: json[r'code'],
        title: json[r'title'],
        description: json[r'description'],
        imageUrl: json[r'imageUrl'],
        sort: json[r'sort'],
        children: ContentSubSection.listFromJson(json[r'children']),
    );

  static List<ContentSection> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ContentSection>[]
      : json.map((v) => ContentSection.fromJson(v)).toList(growable: true == growable);

  static Map<String, ContentSection> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ContentSection>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ContentSection.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ContentSection-objects as value to a dart map
  static Map<String, List<ContentSection>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ContentSection>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ContentSection.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String code,

        String title,

        String description,

        String imageUrl,

        int sort,

        List<ContentSubSection> children,

    }) {
    return ContentSection(
        id: id ?? this.id,
        code: code ?? this.code,
        title: title ?? this.title,
        description: description ?? this.description,
        imageUrl: imageUrl ?? this.imageUrl,
        sort: sort ?? this.sort,
        children: children ?? this.children,
    );
    }


    }

