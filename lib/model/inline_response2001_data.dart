//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse2001Data {
  /// Returns a new [InlineResponse2001Data] instance.
  InlineResponse2001Data({
    this.token,
  });

  String token;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse2001Data &&
     other.token == token;

  @override
  int get hashCode =>
    (token == null ? 0 : token.hashCode);

  @override
  String toString() => 'InlineResponse2001Data[token=$token]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (token != null) {
      json[r'token'] = token;
    }
    return json;
  }

  /// Returns a new [InlineResponse2001Data] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse2001Data fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse2001Data(
        token: json[r'token'],
    );

  static List<InlineResponse2001Data> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse2001Data>[]
      : json.map((v) => InlineResponse2001Data.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse2001Data> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse2001Data>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse2001Data.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse2001Data-objects as value to a dart map
  static Map<String, List<InlineResponse2001Data>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse2001Data>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse2001Data.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String token,

    }) {
    return InlineResponse2001Data(
        token: token ?? this.token,
    );
    }


    }

