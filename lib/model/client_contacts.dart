//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ClientContacts {
  /// Returns a new [ClientContacts] instance.
  ClientContacts({
    this.facebook,
    this.VK,
  });

  String facebook;

  String VK;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ClientContacts &&
     other.facebook == facebook &&
     other.VK == VK;

  @override
  int get hashCode =>
    (facebook == null ? 0 : facebook.hashCode) +
    (VK == null ? 0 : VK.hashCode);

  @override
  String toString() => 'ClientContacts[facebook=$facebook, VK=$VK]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (facebook != null) {
      json[r'Facebook'] = facebook;
    }
    if (VK != null) {
      json[r'VK'] = VK;
    }
    return json;
  }

  /// Returns a new [ClientContacts] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ClientContacts fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ClientContacts(
        facebook: json[r'Facebook'],
        VK: json[r'VK'],
    );

  static List<ClientContacts> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ClientContacts>[]
      : json.map((v) => ClientContacts.fromJson(v)).toList(growable: true == growable);

  static Map<String, ClientContacts> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ClientContacts>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ClientContacts.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ClientContacts-objects as value to a dart map
  static Map<String, List<ClientContacts>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ClientContacts>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ClientContacts.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String facebook,

        String VK,

    }) {
    return ClientContacts(
        facebook: facebook ?? this.facebook,
        VK: VK ?? this.VK,
    );
    }


    }

