//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class NotificationDBOType {
  /// Returns a new [NotificationDBOType] instance.
  NotificationDBOType({
    this.id,
    this.name,
  });

  /// Код типа уведомления
  NotificationDBOTypeIdEnum id;

  /// Название типа уведомления
  String name;

  @override
  bool operator ==(Object other) => identical(this, other) || other is NotificationDBOType &&
     other.id == id &&
     other.name == name;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode);

  @override
  String toString() => 'NotificationDBOType[id=$id, name=$name]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    return json;
  }

  /// Returns a new [NotificationDBOType] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static NotificationDBOType fromJson(Map<String, dynamic> json) => json == null
    ? null
    : NotificationDBOType(
        id: NotificationDBOTypeIdEnum.fromJson(json[r'id']),
        name: json[r'name'],
    );

  static List<NotificationDBOType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <NotificationDBOType>[]
      : json.map((v) => NotificationDBOType.fromJson(v)).toList(growable: true == growable);

  static Map<String, NotificationDBOType> mapFromJson(Map<String, dynamic> json) {
    final map = <String, NotificationDBOType>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = NotificationDBOType.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of NotificationDBOType-objects as value to a dart map
  static Map<String, List<NotificationDBOType>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<NotificationDBOType>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = NotificationDBOType.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        NotificationDBOTypeIdEnum id,

        String name,

    }) {
    return NotificationDBOType(
        id: id ?? this.id,
        name: name ?? this.name,
    );
    }


    }

/// Код типа уведомления
class NotificationDBOTypeIdEnum {
  /// Instantiate a new enum with the provided [value].
  const NotificationDBOTypeIdEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  bool operator ==(Object other) => identical(this, other) ||
      other is NotificationDBOTypeIdEnum && other.value == value;

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() => value;

  String toJson() => value;

  static const coupon = NotificationDBOTypeIdEnum._(r'coupon');
  static const info = NotificationDBOTypeIdEnum._(r'info');

  /// List of all possible values in this [enum][NotificationDBOTypeIdEnum].
  static const values = <NotificationDBOTypeIdEnum>[
    coupon,
    info,
  ];

  static NotificationDBOTypeIdEnum fromJson(dynamic value) =>
    NotificationDBOTypeIdEnumTypeTransformer().decode(value);

  static List<NotificationDBOTypeIdEnum> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <NotificationDBOTypeIdEnum>[]
      : json
          .map((value) => NotificationDBOTypeIdEnum.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [NotificationDBOTypeIdEnum] to String,
/// and [decode] dynamic data back to [NotificationDBOTypeIdEnum].
class NotificationDBOTypeIdEnumTypeTransformer {
  const NotificationDBOTypeIdEnumTypeTransformer._();

  factory NotificationDBOTypeIdEnumTypeTransformer() => _instance ??= NotificationDBOTypeIdEnumTypeTransformer._();

  String encode(NotificationDBOTypeIdEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a NotificationDBOTypeIdEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  NotificationDBOTypeIdEnum decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'coupon': return NotificationDBOTypeIdEnum.coupon;
      case r'info': return NotificationDBOTypeIdEnum.info;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [NotificationDBOTypeIdEnumTypeTransformer] instance.
  static NotificationDBOTypeIdEnumTypeTransformer _instance;
}

