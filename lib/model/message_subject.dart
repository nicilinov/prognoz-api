//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class MessageSubject {
  /// Returns a new [MessageSubject] instance.
  MessageSubject({
    this.id,
    this.name,
  });

  /// Код темы сообщения
  String id;

  /// Название темы сообщения
  String name;

  @override
  bool operator ==(Object other) => identical(this, other) || other is MessageSubject &&
     other.id == id &&
     other.name == name;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode);

  @override
  String toString() => 'MessageSubject[id=$id, name=$name]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    return json;
  }

  /// Returns a new [MessageSubject] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static MessageSubject fromJson(Map<String, dynamic> json) => json == null
    ? null
    : MessageSubject(
        id: json[r'id'],
        name: json[r'name'],
    );

  static List<MessageSubject> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <MessageSubject>[]
      : json.map((v) => MessageSubject.fromJson(v)).toList(growable: true == growable);

  static Map<String, MessageSubject> mapFromJson(Map<String, dynamic> json) {
    final map = <String, MessageSubject>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = MessageSubject.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of MessageSubject-objects as value to a dart map
  static Map<String, List<MessageSubject>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<MessageSubject>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = MessageSubject.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String id,

        String name,

    }) {
    return MessageSubject(
        id: id ?? this.id,
        name: name ?? this.name,
    );
    }


    }

