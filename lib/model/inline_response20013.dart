//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse20013 {
  /// Returns a new [InlineResponse20013] instance.
  InlineResponse20013({
    this.data,
    this.meta,
  });

  ContentSection data;

  MetaPagination meta;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse20013 &&
     other.data == data &&
     other.meta == meta;

  @override
  int get hashCode =>
    (data == null ? 0 : data.hashCode) +
    (meta == null ? 0 : meta.hashCode);

  @override
  String toString() => 'InlineResponse20013[data=$data, meta=$meta]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (data != null) {
      json[r'data'] = data;
    }
    if (meta != null) {
      json[r'meta'] = meta;
    }
    return json;
  }

  /// Returns a new [InlineResponse20013] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse20013 fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse20013(
        data: ContentSection.fromJson(json[r'data']),
        meta: MetaPagination.fromJson(json[r'meta']),
    );

  static List<InlineResponse20013> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse20013>[]
      : json.map((v) => InlineResponse20013.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse20013> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse20013>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse20013.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse20013-objects as value to a dart map
  static Map<String, List<InlineResponse20013>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse20013>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse20013.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        ContentSection data,

        MetaPagination meta,

    }) {
    return InlineResponse20013(
        data: data ?? this.data,
        meta: meta ?? this.meta,
    );
    }


    }

