//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class NotificationDBO {
  /// Returns a new [NotificationDBO] instance.
  NotificationDBO({
    this.id,
    this.title,
    this.message,
    this.type,
    this.sentAt,
    this.read,
  });

  /// Id сообщения
  String id;

  /// Заголовок сообщения
  String title;

  /// Текст сообщения
  String message;

  NotificationType type;

  /// Дата и время отправки сообщения в формате 2020-11-08T00:00:00
  String sentAt;

  /// Признак прочтения
  bool read;

  @override
  bool operator ==(Object other) => identical(this, other) || other is NotificationDBO &&
     other.id == id &&
     other.title == title &&
     other.message == message &&
     other.type == type &&
     other.sentAt == sentAt &&
     other.read == read;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (title == null ? 0 : title.hashCode) +
    (message == null ? 0 : message.hashCode) +
    (type == null ? 0 : type.hashCode) +
    (sentAt == null ? 0 : sentAt.hashCode) +
    (read == null ? 0 : read.hashCode);

  @override
  String toString() => 'NotificationDBO[id=$id, title=$title, message=$message, type=$type, sentAt=$sentAt, read=$read]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (title != null) {
      json[r'title'] = title;
    }
    if (message != null) {
      json[r'message'] = message;
    }
    if (type != null) {
      json[r'type'] = type;
    }
    if (sentAt != null) {
      json[r'sentAt'] = sentAt;
    }
    if (read != null) {
      json[r'read'] = read;
    }
    return json;
  }

  /// Returns a new [NotificationDBO] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static NotificationDBO fromJson(Map<String, dynamic> json) => json == null
    ? null
    : NotificationDBO(
        id: json[r'id'],
        title: json[r'title'],
        message: json[r'message'],
        type: NotificationType.fromJson(json[r'type']),
        sentAt: json[r'sentAt'],
        read: json[r'read'],
    );

  static List<NotificationDBO> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <NotificationDBO>[]
      : json.map((v) => NotificationDBO.fromJson(v)).toList(growable: true == growable);

  static Map<String, NotificationDBO> mapFromJson(Map<String, dynamic> json) {
    final map = <String, NotificationDBO>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = NotificationDBO.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of NotificationDBO-objects as value to a dart map
  static Map<String, List<NotificationDBO>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<NotificationDBO>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = NotificationDBO.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String id,

        String title,

        String message,

        NotificationType type,

        String sentAt,

        bool read,

    }) {
    return NotificationDBO(
        id: id ?? this.id,
        title: title ?? this.title,
        message: message ?? this.message,
        type: type ?? this.type,
        sentAt: sentAt ?? this.sentAt,
        read: read ?? this.read,
    );
    }


    }

