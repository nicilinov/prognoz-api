//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse2003Data {
  /// Returns a new [InlineResponse2003Data] instance.
  InlineResponse2003Data({
    this.qrcode,
  });

  /// Временный QR-код клиента для авторизации на кассе, формат изображения base64
  String qrcode;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse2003Data &&
     other.qrcode == qrcode;

  @override
  int get hashCode =>
    (qrcode == null ? 0 : qrcode.hashCode);

  @override
  String toString() => 'InlineResponse2003Data[qrcode=$qrcode]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (qrcode != null) {
      json[r'qrcode'] = qrcode;
    }
    return json;
  }

  /// Returns a new [InlineResponse2003Data] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse2003Data fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse2003Data(
        qrcode: json[r'qrcode'],
    );

  static List<InlineResponse2003Data> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse2003Data>[]
      : json.map((v) => InlineResponse2003Data.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse2003Data> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse2003Data>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse2003Data.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse2003Data-objects as value to a dart map
  static Map<String, List<InlineResponse2003Data>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse2003Data>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse2003Data.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String qrcode,

    }) {
    return InlineResponse2003Data(
        qrcode: qrcode ?? this.qrcode,
    );
    }


    }

