//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class CouponGallery {
  /// Returns a new [CouponGallery] instance.
  CouponGallery({
    this.id,
    this.url,
  });

  /// Id картинки
  String id;

  /// URL картинки
  String url;

  @override
  bool operator ==(Object other) => identical(this, other) || other is CouponGallery &&
     other.id == id &&
     other.url == url;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (url == null ? 0 : url.hashCode);

  @override
  String toString() => 'CouponGallery[id=$id, url=$url]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (url != null) {
      json[r'url'] = url;
    }
    return json;
  }

  /// Returns a new [CouponGallery] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static CouponGallery fromJson(Map<String, dynamic> json) => json == null
    ? null
    : CouponGallery(
        id: json[r'id'],
        url: json[r'url'],
    );

  static List<CouponGallery> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <CouponGallery>[]
      : json.map((v) => CouponGallery.fromJson(v)).toList(growable: true == growable);

  static Map<String, CouponGallery> mapFromJson(Map<String, dynamic> json) {
    final map = <String, CouponGallery>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = CouponGallery.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of CouponGallery-objects as value to a dart map
  static Map<String, List<CouponGallery>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<CouponGallery>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = CouponGallery.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String id,

        String url,

    }) {
    return CouponGallery(
        id: id ?? this.id,
        url: url ?? this.url,
    );
    }


    }

