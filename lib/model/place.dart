//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Place {
  /// Returns a new [Place] instance.
  Place({
    this.id,
    this.name,
    this.address,
    this.latitude,
    this.longtitude,
    this.services = const [],
    this.products = const [],
  });

  /// Id торговой точки
  String id;

  /// Название торговой точки
  String name;

  /// Адрес торговой точки
  String address;

  /// Широта торговой точки
  num latitude;

  /// Долгота торговой точки
  num longtitude;

  List<PlaceService> services;

  List<Product> products;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Place &&
     other.id == id &&
     other.name == name &&
     other.address == address &&
     other.latitude == latitude &&
     other.longtitude == longtitude &&
     other.services == services &&
     other.products == products;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (address == null ? 0 : address.hashCode) +
    (latitude == null ? 0 : latitude.hashCode) +
    (longtitude == null ? 0 : longtitude.hashCode) +
    (services == null ? 0 : services.hashCode) +
    (products == null ? 0 : products.hashCode);

  @override
  String toString() => 'Place[id=$id, name=$name, address=$address, latitude=$latitude, longtitude=$longtitude, services=$services, products=$products]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (address != null) {
      json[r'address'] = address;
    }
    if (latitude != null) {
      json[r'latitude'] = latitude;
    }
    if (longtitude != null) {
      json[r'longtitude'] = longtitude;
    }
    if (services != null) {
      json[r'services'] = services;
    }
    if (products != null) {
      json[r'products'] = products;
    }
    return json;
  }

  /// Returns a new [Place] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Place fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Place(
        id: json[r'id'],
        name: json[r'name'],
        address: json[r'address'],
        latitude: json[r'latitude'] == null ?
          null :
          json[r'latitude'].toDouble(),
        longtitude: json[r'longtitude'] == null ?
          null :
          json[r'longtitude'].toDouble(),
        services: PlaceService.listFromJson(json[r'services']),
        products: Product.listFromJson(json[r'products']),
    );

  static List<Place> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Place>[]
      : json.map((v) => Place.fromJson(v)).toList(growable: true == growable);

  static Map<String, Place> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Place>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Place.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Place-objects as value to a dart map
  static Map<String, List<Place>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Place>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Place.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String id,

        String name,

        String address,

        num latitude,

        num longtitude,

        List<PlaceService> services,

        List<Product> products,

    }) {
    return Place(
        id: id ?? this.id,
        name: name ?? this.name,
        address: address ?? this.address,
        latitude: latitude ?? this.latitude,
        longtitude: longtitude ?? this.longtitude,
        services: services ?? this.services,
        products: products ?? this.products,
    );
    }


    }

