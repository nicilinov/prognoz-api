//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse2005 {
  /// Returns a new [InlineResponse2005] instance.
  InlineResponse2005({
    this.data = const [],
    this.meta,
  });

  List<Coupon> data;

  MetaPagination meta;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse2005 &&
     other.data == data &&
     other.meta == meta;

  @override
  int get hashCode =>
    (data == null ? 0 : data.hashCode) +
    (meta == null ? 0 : meta.hashCode);

  @override
  String toString() => 'InlineResponse2005[data=$data, meta=$meta]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (data != null) {
      json[r'data'] = data;
    }
    if (meta != null) {
      json[r'meta'] = meta;
    }
    return json;
  }

  /// Returns a new [InlineResponse2005] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse2005 fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse2005(
        data: Coupon.listFromJson(json[r'data']),
        meta: MetaPagination.fromJson(json[r'meta']),
    );

  static List<InlineResponse2005> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse2005>[]
      : json.map((v) => InlineResponse2005.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse2005> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse2005>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse2005.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse2005-objects as value to a dart map
  static Map<String, List<InlineResponse2005>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse2005>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse2005.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        List<Coupon> data,

        MetaPagination meta,

    }) {
    return InlineResponse2005(
        data: data ?? this.data,
        meta: meta ?? this.meta,
    );
    }


    }

