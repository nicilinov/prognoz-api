//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse20011 {
  /// Returns a new [InlineResponse20011] instance.
  InlineResponse20011({
    this.data = const [],
  });

  List<PlaceService> data;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse20011 &&
     other.data == data;

  @override
  int get hashCode =>
    (data == null ? 0 : data.hashCode);

  @override
  String toString() => 'InlineResponse20011[data=$data]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (data != null) {
      json[r'data'] = data;
    }
    return json;
  }

  /// Returns a new [InlineResponse20011] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse20011 fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse20011(
        data: PlaceService.listFromJson(json[r'data']),
    );

  static List<InlineResponse20011> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse20011>[]
      : json.map((v) => InlineResponse20011.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse20011> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse20011>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse20011.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse20011-objects as value to a dart map
  static Map<String, List<InlineResponse20011>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse20011>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse20011.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        List<PlaceService> data,

    }) {
    return InlineResponse20011(
        data: data ?? this.data,
    );
    }


    }

