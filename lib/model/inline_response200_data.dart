//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse200Data {
  /// Returns a new [InlineResponse200Data] instance.
  InlineResponse200Data({
    this.message,
  });

  String message;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse200Data &&
     other.message == message;

  @override
  int get hashCode =>
    (message == null ? 0 : message.hashCode);

  @override
  String toString() => 'InlineResponse200Data[message=$message]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (message != null) {
      json[r'message'] = message;
    }
    return json;
  }

  /// Returns a new [InlineResponse200Data] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse200Data fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse200Data(
        message: json[r'message'],
    );

  static List<InlineResponse200Data> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse200Data>[]
      : json.map((v) => InlineResponse200Data.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse200Data> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse200Data>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse200Data.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse200Data-objects as value to a dart map
  static Map<String, List<InlineResponse200Data>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse200Data>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse200Data.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String message,

    }) {
    return InlineResponse200Data(
        message: message ?? this.message,
    );
    }


    }

