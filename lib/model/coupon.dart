//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Coupon {
  /// Returns a new [Coupon] instance.
  Coupon({
    this.id,
    this.name,
    this.description,
    this.finishedAt,
    this.finishedAtLabel,
    this.placeIds = const [],
    this.imageUrl,
    this.galleryUrls = const [],
    this.number,
    this.barcode,
    this.remind,
    this.productId,
    this.productExternalId,
    this.productName,
    this.price,
    this.productTypeId,
  });

  int id;

  /// Название купона
  String name;

  /// Описание купона
  String description;

  /// Дата окончания в формате 2020-11-08T00:00:00
  String finishedAt;

  /// Надпись даты окончания в формате до 25 января 2020
  String finishedAtLabel;

  /// Id торговых точек
  List<String> placeIds;

  /// Ссылка на файл изображения купона
  String imageUrl;

  List<String> galleryUrls;

  /// код купона из 10 цифр
  String number;

  /// Шрих-код купона в формате изображения в base64
  String barcode;

  /// Признак уведомлять ли о купоне при приближении к АЗС
  bool remind;

  /// Id товара
  String productId;

  /// Номенклатурный номер товара
  String productExternalId;

  /// Название товара
  String productName;

  /// Цена товара по купону
  String price;

  /// Id типа товара. Если равно 1, то это цена любимого топлива клиента
  int productTypeId;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Coupon &&
     other.id == id &&
     other.name == name &&
     other.description == description &&
     other.finishedAt == finishedAt &&
     other.finishedAtLabel == finishedAtLabel &&
     other.placeIds == placeIds &&
     other.imageUrl == imageUrl &&
     other.galleryUrls == galleryUrls &&
     other.number == number &&
     other.barcode == barcode &&
     other.remind == remind &&
     other.productId == productId &&
     other.productExternalId == productExternalId &&
     other.productName == productName &&
     other.price == price &&
     other.productTypeId == productTypeId;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (description == null ? 0 : description.hashCode) +
    (finishedAt == null ? 0 : finishedAt.hashCode) +
    (finishedAtLabel == null ? 0 : finishedAtLabel.hashCode) +
    (placeIds == null ? 0 : placeIds.hashCode) +
    (imageUrl == null ? 0 : imageUrl.hashCode) +
    (galleryUrls == null ? 0 : galleryUrls.hashCode) +
    (number == null ? 0 : number.hashCode) +
    (barcode == null ? 0 : barcode.hashCode) +
    (remind == null ? 0 : remind.hashCode) +
    (productId == null ? 0 : productId.hashCode) +
    (productExternalId == null ? 0 : productExternalId.hashCode) +
    (productName == null ? 0 : productName.hashCode) +
    (price == null ? 0 : price.hashCode) +
    (productTypeId == null ? 0 : productTypeId.hashCode);

  @override
  String toString() => 'Coupon[id=$id, name=$name, description=$description, finishedAt=$finishedAt, finishedAtLabel=$finishedAtLabel, placeIds=$placeIds, imageUrl=$imageUrl, galleryUrls=$galleryUrls, number=$number, barcode=$barcode, remind=$remind, productId=$productId, productExternalId=$productExternalId, productName=$productName, price=$price, productTypeId=$productTypeId]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (description != null) {
      json[r'description'] = description;
    }
    if (finishedAt != null) {
      json[r'finishedAt'] = finishedAt;
    }
    if (finishedAtLabel != null) {
      json[r'finishedAtLabel'] = finishedAtLabel;
    }
    if (placeIds != null) {
      json[r'placeIds'] = placeIds;
    }
    if (imageUrl != null) {
      json[r'imageUrl'] = imageUrl;
    }
    if (galleryUrls != null) {
      json[r'galleryUrls'] = galleryUrls;
    }
    if (number != null) {
      json[r'number'] = number;
    }
    if (barcode != null) {
      json[r'barcode'] = barcode;
    }
    if (remind != null) {
      json[r'remind'] = remind;
    }
    if (productId != null) {
      json[r'productId'] = productId;
    }
    if (productExternalId != null) {
      json[r'productExternalId'] = productExternalId;
    }
    if (productName != null) {
      json[r'productName'] = productName;
    }
    if (price != null) {
      json[r'price'] = price;
    }
    if (productTypeId != null) {
      json[r'productTypeId'] = productTypeId;
    }
    return json;
  }

  /// Returns a new [Coupon] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Coupon fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Coupon(
        id: json[r'id'],
        name: json[r'name'],
        description: json[r'description'],
        finishedAt: json[r'finishedAt'],
        finishedAtLabel: json[r'finishedAtLabel'],
        placeIds: json[r'placeIds'] == null
          ? null
          : (json[r'placeIds'] as List).cast<String>(),
        imageUrl: json[r'imageUrl'],
        galleryUrls: json[r'galleryUrls'] == null
          ? null
          : (json[r'galleryUrls'] as List).cast<String>(),
        number: json[r'number'],
        barcode: json[r'barcode'],
        remind: json[r'remind'],
        productId: json[r'productId'],
        productExternalId: json[r'productExternalId'],
        productName: json[r'productName'],
        price: json[r'price'],
        productTypeId: json[r'productTypeId'],
    );

  static List<Coupon> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Coupon>[]
      : json.map((v) => Coupon.fromJson(v)).toList(growable: true == growable);

  static Map<String, Coupon> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Coupon>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Coupon.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Coupon-objects as value to a dart map
  static Map<String, List<Coupon>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Coupon>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Coupon.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String name,

        String description,

        String finishedAt,

        String finishedAtLabel,

        List<String> placeIds,

        String imageUrl,

        List<String> galleryUrls,

        String number,

        String barcode,

        bool remind,

        String productId,

        String productExternalId,

        String productName,

        String price,

        int productTypeId,

    }) {
    return Coupon(
        id: id ?? this.id,
        name: name ?? this.name,
        description: description ?? this.description,
        finishedAt: finishedAt ?? this.finishedAt,
        finishedAtLabel: finishedAtLabel ?? this.finishedAtLabel,
        placeIds: placeIds ?? this.placeIds,
        imageUrl: imageUrl ?? this.imageUrl,
        galleryUrls: galleryUrls ?? this.galleryUrls,
        number: number ?? this.number,
        barcode: barcode ?? this.barcode,
        remind: remind ?? this.remind,
        productId: productId ?? this.productId,
        productExternalId: productExternalId ?? this.productExternalId,
        productName: productName ?? this.productName,
        price: price ?? this.price,
        productTypeId: productTypeId ?? this.productTypeId,
    );
    }


    }

