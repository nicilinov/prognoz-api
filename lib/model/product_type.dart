//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ProductType {
  /// Returns a new [ProductType] instance.
  ProductType({
    this.id,
    this.name,
  });

  /// Код типа товара
  int id;

  /// Название типа товара
  String name;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ProductType &&
     other.id == id &&
     other.name == name;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode);

  @override
  String toString() => 'ProductType[id=$id, name=$name]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    return json;
  }

  /// Returns a new [ProductType] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ProductType fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ProductType(
        id: json[r'id'],
        name: json[r'name'],
    );

  static List<ProductType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ProductType>[]
      : json.map((v) => ProductType.fromJson(v)).toList(growable: true == growable);

  static Map<String, ProductType> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ProductType>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ProductType.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ProductType-objects as value to a dart map
  static Map<String, List<ProductType>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ProductType>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ProductType.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        int id,

        String name,

    }) {
    return ProductType(
        id: id ?? this.id,
        name: name ?? this.name,
    );
    }


    }

