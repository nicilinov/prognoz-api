//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class ModelClient {
  /// Returns a new [ModelClient] instance.
  ModelClient({
    this.cardId,
    this.phoneNumber,
    this.email,
    this.isEmailConfirmed,
    this.city,
    this.status,
    this.fio,
    this.name,
    this.surname,
    this.balance,
    this.favoriteFuelId,
    this.favoriteFuelName,
    this.createdAt,
    this.VK,
    this.facebook,
  });

  /// Номер карты лояльности клиента  (только просмотр)
  String cardId;

  /// Номер телефона клиента
  String phoneNumber;

  /// E-mail клиента
  String email;

  /// E-mail клиента подтверждён?  (только просмотр)
  bool isEmailConfirmed;

  /// Город клиента
  String city;

  ClientStatus status;

  /// Ф.И.О. клиента  (только просмотр)
  String fio;

  /// Имя клиента
  String name;

  /// Фамилия клиента
  String surname;

  /// Баланс клиента  (только просмотр)
  int balance;

  /// Id любимого вида топлива клиента
  String favoriteFuelId;

  /// Название любимого вид топлива клиента (только просмотр)
  String favoriteFuelName;

  /// Дата регистрации  (только просмотр)
  String createdAt;

  /// VK клиента
  String VK;

  /// Facebook клиента
  String facebook;

  @override
  bool operator ==(Object other) => identical(this, other) || other is ModelClient &&
     other.cardId == cardId &&
     other.phoneNumber == phoneNumber &&
     other.email == email &&
     other.isEmailConfirmed == isEmailConfirmed &&
     other.city == city &&
     other.status == status &&
     other.fio == fio &&
     other.name == name &&
     other.surname == surname &&
     other.balance == balance &&
     other.favoriteFuelId == favoriteFuelId &&
     other.favoriteFuelName == favoriteFuelName &&
     other.createdAt == createdAt &&
     other.VK == VK &&
     other.facebook == facebook;

  @override
  int get hashCode =>
    (cardId == null ? 0 : cardId.hashCode) +
    (phoneNumber == null ? 0 : phoneNumber.hashCode) +
    (email == null ? 0 : email.hashCode) +
    (isEmailConfirmed == null ? 0 : isEmailConfirmed.hashCode) +
    (city == null ? 0 : city.hashCode) +
    (status == null ? 0 : status.hashCode) +
    (fio == null ? 0 : fio.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (surname == null ? 0 : surname.hashCode) +
    (balance == null ? 0 : balance.hashCode) +
    (favoriteFuelId == null ? 0 : favoriteFuelId.hashCode) +
    (favoriteFuelName == null ? 0 : favoriteFuelName.hashCode) +
    (createdAt == null ? 0 : createdAt.hashCode) +
    (VK == null ? 0 : VK.hashCode) +
    (facebook == null ? 0 : facebook.hashCode);

  @override
  String toString() => 'ModelClient[cardId=$cardId, phoneNumber=$phoneNumber, email=$email, isEmailConfirmed=$isEmailConfirmed, city=$city, status=$status, fio=$fio, name=$name, surname=$surname, balance=$balance, favoriteFuelId=$favoriteFuelId, favoriteFuelName=$favoriteFuelName, createdAt=$createdAt, VK=$VK, facebook=$facebook]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (cardId != null) {
      json[r'cardId'] = cardId;
    }
    if (phoneNumber != null) {
      json[r'phoneNumber'] = phoneNumber;
    }
    if (email != null) {
      json[r'email'] = email;
    }
    if (isEmailConfirmed != null) {
      json[r'isEmailConfirmed'] = isEmailConfirmed;
    }
    if (city != null) {
      json[r'city'] = city;
    }
    if (status != null) {
      json[r'status'] = status;
    }
    if (fio != null) {
      json[r'fio'] = fio;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (surname != null) {
      json[r'surname'] = surname;
    }
    if (balance != null) {
      json[r'balance'] = balance;
    }
    if (favoriteFuelId != null) {
      json[r'favoriteFuelId'] = favoriteFuelId;
    }
    if (favoriteFuelName != null) {
      json[r'favoriteFuelName'] = favoriteFuelName;
    }
    if (createdAt != null) {
      json[r'createdAt'] = createdAt;
    }
    if (VK != null) {
      json[r'VK'] = VK;
    }
    if (facebook != null) {
      json[r'Facebook'] = facebook;
    }
    return json;
  }

  /// Returns a new [ModelClient] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static ModelClient fromJson(Map<String, dynamic> json) => json == null
    ? null
    : ModelClient(
        cardId: json[r'cardId'],
        phoneNumber: json[r'phoneNumber'],
        email: json[r'email'],
        isEmailConfirmed: json[r'isEmailConfirmed'],
        city: json[r'city'],
        status: ClientStatus.fromJson(json[r'status']),
        fio: json[r'fio'],
        name: json[r'name'],
        surname: json[r'surname'],
        balance: json[r'balance'],
        favoriteFuelId: json[r'favoriteFuelId'],
        favoriteFuelName: json[r'favoriteFuelName'],
        createdAt: json[r'createdAt'],
        VK: json[r'VK'],
        facebook: json[r'Facebook'],
    );

  static List<ModelClient> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <ModelClient>[]
      : json.map((v) => ModelClient.fromJson(v)).toList(growable: true == growable);

  static Map<String, ModelClient> mapFromJson(Map<String, dynamic> json) {
    final map = <String, ModelClient>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = ModelClient.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of ModelClient-objects as value to a dart map
  static Map<String, List<ModelClient>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<ModelClient>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = ModelClient.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String cardId,

        String phoneNumber,

        String email,

        bool isEmailConfirmed,

        String city,

        ClientStatus status,

        String fio,

        String name,

        String surname,

        int balance,

        String favoriteFuelId,

        String favoriteFuelName,

        String createdAt,

        String VK,

        String facebook,

    }) {
    return ModelClient(
        cardId: cardId ?? this.cardId,
        phoneNumber: phoneNumber ?? this.phoneNumber,
        email: email ?? this.email,
        isEmailConfirmed: isEmailConfirmed ?? this.isEmailConfirmed,
        city: city ?? this.city,
        status: status ?? this.status,
        fio: fio ?? this.fio,
        name: name ?? this.name,
        surname: surname ?? this.surname,
        balance: balance ?? this.balance,
        favoriteFuelId: favoriteFuelId ?? this.favoriteFuelId,
        favoriteFuelName: favoriteFuelName ?? this.favoriteFuelName,
        createdAt: createdAt ?? this.createdAt,
        VK: VK ?? this.VK,
        facebook: facebook ?? this.facebook,
    );
    }


    }

