//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class NotificationType {
  /// Returns a new [NotificationType] instance.
  NotificationType({
    this.id,
    this.name,
  });

  /// Код типа уведомления
  NotificationTypeIdEnum id;

  /// Название типа уведомления
  String name;

  @override
  bool operator ==(Object other) => identical(this, other) || other is NotificationType &&
     other.id == id &&
     other.name == name;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (name == null ? 0 : name.hashCode);

  @override
  String toString() => 'NotificationType[id=$id, name=$name]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    return json;
  }

  /// Returns a new [NotificationType] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static NotificationType fromJson(Map<String, dynamic> json) => json == null
    ? null
    : NotificationType(
        id: NotificationTypeIdEnum.fromJson(json[r'id']),
        name: json[r'name'],
    );

  static List<NotificationType> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <NotificationType>[]
      : json.map((v) => NotificationType.fromJson(v)).toList(growable: true == growable);

  static Map<String, NotificationType> mapFromJson(Map<String, dynamic> json) {
    final map = <String, NotificationType>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = NotificationType.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of NotificationType-objects as value to a dart map
  static Map<String, List<NotificationType>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<NotificationType>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = NotificationType.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        NotificationTypeIdEnum id,

        String name,

    }) {
    return NotificationType(
        id: id ?? this.id,
        name: name ?? this.name,
    );
    }


    }

/// Код типа уведомления
class NotificationTypeIdEnum {
  /// Instantiate a new enum with the provided [value].
  const NotificationTypeIdEnum._(this.value);

  /// The underlying value of this enum member.
  final String value;

  @override
  bool operator ==(Object other) => identical(this, other) ||
      other is NotificationTypeIdEnum && other.value == value;

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() => value;

  String toJson() => value;

  static const coupon = NotificationTypeIdEnum._(r'coupon');
  static const info = NotificationTypeIdEnum._(r'info');

  /// List of all possible values in this [enum][NotificationTypeIdEnum].
  static const values = <NotificationTypeIdEnum>[
    coupon,
    info,
  ];

  static NotificationTypeIdEnum fromJson(dynamic value) =>
    NotificationTypeIdEnumTypeTransformer().decode(value);

  static List<NotificationTypeIdEnum> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <NotificationTypeIdEnum>[]
      : json
          .map((value) => NotificationTypeIdEnum.fromJson(value))
          .toList(growable: true == growable);
}

/// Transformation class that can [encode] an instance of [NotificationTypeIdEnum] to String,
/// and [decode] dynamic data back to [NotificationTypeIdEnum].
class NotificationTypeIdEnumTypeTransformer {
  const NotificationTypeIdEnumTypeTransformer._();

  factory NotificationTypeIdEnumTypeTransformer() => _instance ??= NotificationTypeIdEnumTypeTransformer._();

  String encode(NotificationTypeIdEnum data) => data.value;

  /// Decodes a [dynamic value][data] to a NotificationTypeIdEnum.
  ///
  /// If [allowNull] is true and the [dynamic value][data] cannot be decoded successfully,
  /// then null is returned. However, if [allowNull] is false and the [dynamic value][data]
  /// cannot be decoded successfully, then an [UnimplementedError] is thrown.
  ///
  /// The [allowNull] is very handy when an API changes and a new enum value is added or removed,
  /// and users are still using an old app with the old code.
  NotificationTypeIdEnum decode(dynamic data, {bool allowNull}) {
    switch (data) {
      case r'coupon': return NotificationTypeIdEnum.coupon;
      case r'info': return NotificationTypeIdEnum.info;
      default:
        if (allowNull == false) {
          throw ArgumentError('Unknown enum value to decode: $data');
        }
    }
    return null;
  }

  /// Singleton [NotificationTypeIdEnumTypeTransformer] instance.
  static NotificationTypeIdEnumTypeTransformer _instance;
}

