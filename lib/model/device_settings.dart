//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class DeviceSettings {
  /// Returns a new [DeviceSettings] instance.
  DeviceSettings({
    this.geoEnabled,
    this.notificationsEnabled,
    this.adEnabled,
    this.personalOffersEnabled,
    this.techInfoEnabled,
  });

  /// Геолокация включена
  bool geoEnabled;

  /// Все уведомления включены
  bool notificationsEnabled;

  /// Все рекламные акции включены
  bool adEnabled;

  /// Индивидуальные предложения включены
  bool personalOffersEnabled;

  /// Техническая информация включена
  bool techInfoEnabled;

  @override
  bool operator ==(Object other) => identical(this, other) || other is DeviceSettings &&
     other.geoEnabled == geoEnabled &&
     other.notificationsEnabled == notificationsEnabled &&
     other.adEnabled == adEnabled &&
     other.personalOffersEnabled == personalOffersEnabled &&
     other.techInfoEnabled == techInfoEnabled;

  @override
  int get hashCode =>
    (geoEnabled == null ? 0 : geoEnabled.hashCode) +
    (notificationsEnabled == null ? 0 : notificationsEnabled.hashCode) +
    (adEnabled == null ? 0 : adEnabled.hashCode) +
    (personalOffersEnabled == null ? 0 : personalOffersEnabled.hashCode) +
    (techInfoEnabled == null ? 0 : techInfoEnabled.hashCode);

  @override
  String toString() => 'DeviceSettings[geoEnabled=$geoEnabled, notificationsEnabled=$notificationsEnabled, adEnabled=$adEnabled, personalOffersEnabled=$personalOffersEnabled, techInfoEnabled=$techInfoEnabled]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (geoEnabled != null) {
      json[r'geoEnabled'] = geoEnabled;
    }
    if (notificationsEnabled != null) {
      json[r'notificationsEnabled'] = notificationsEnabled;
    }
    if (adEnabled != null) {
      json[r'adEnabled'] = adEnabled;
    }
    if (personalOffersEnabled != null) {
      json[r'personalOffersEnabled'] = personalOffersEnabled;
    }
    if (techInfoEnabled != null) {
      json[r'techInfoEnabled'] = techInfoEnabled;
    }
    return json;
  }

  /// Returns a new [DeviceSettings] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static DeviceSettings fromJson(Map<String, dynamic> json) => json == null
    ? null
    : DeviceSettings(
        geoEnabled: json[r'geoEnabled'],
        notificationsEnabled: json[r'notificationsEnabled'],
        adEnabled: json[r'adEnabled'],
        personalOffersEnabled: json[r'personalOffersEnabled'],
        techInfoEnabled: json[r'techInfoEnabled'],
    );

  static List<DeviceSettings> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <DeviceSettings>[]
      : json.map((v) => DeviceSettings.fromJson(v)).toList(growable: true == growable);

  static Map<String, DeviceSettings> mapFromJson(Map<String, dynamic> json) {
    final map = <String, DeviceSettings>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = DeviceSettings.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of DeviceSettings-objects as value to a dart map
  static Map<String, List<DeviceSettings>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<DeviceSettings>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = DeviceSettings.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        bool geoEnabled,

        bool notificationsEnabled,

        bool adEnabled,

        bool personalOffersEnabled,

        bool techInfoEnabled,

    }) {
    return DeviceSettings(
        geoEnabled: geoEnabled ?? this.geoEnabled,
        notificationsEnabled: notificationsEnabled ?? this.notificationsEnabled,
        adEnabled: adEnabled ?? this.adEnabled,
        personalOffersEnabled: personalOffersEnabled ?? this.personalOffersEnabled,
        techInfoEnabled: techInfoEnabled ?? this.techInfoEnabled,
    );
    }


    }

