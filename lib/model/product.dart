//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class Product {
  /// Returns a new [Product] instance.
  Product({
    this.id,
    this.externalId,
    this.name,
    this.type,
    this.isBonus,
    this.bonusAdd = const [],
    this.bonusDel = const [],
    this.bonusAddLabel,
    this.bonusDelLabel,
  });

  String id;

  /// Номенклатурный номер товара
  String externalId;

  /// Название товара
  String name;

  ProductType type;

  /// Признак участия в бонусной программе
  bool isBonus;

  /// Коэффициенты накопления баллов
  List<num> bonusAdd;

  /// Коэффициенты списания баллов
  List<num> bonusDel;

  /// Коэффициенты накопления баллов на просмотре товара
  String bonusAddLabel;

  /// Коэффициенты списания баллов на просмотре товара
  String bonusDelLabel;

  @override
  bool operator ==(Object other) => identical(this, other) || other is Product &&
     other.id == id &&
     other.externalId == externalId &&
     other.name == name &&
     other.type == type &&
     other.isBonus == isBonus &&
     other.bonusAdd == bonusAdd &&
     other.bonusDel == bonusDel &&
     other.bonusAddLabel == bonusAddLabel &&
     other.bonusDelLabel == bonusDelLabel;

  @override
  int get hashCode =>
    (id == null ? 0 : id.hashCode) +
    (externalId == null ? 0 : externalId.hashCode) +
    (name == null ? 0 : name.hashCode) +
    (type == null ? 0 : type.hashCode) +
    (isBonus == null ? 0 : isBonus.hashCode) +
    (bonusAdd == null ? 0 : bonusAdd.hashCode) +
    (bonusDel == null ? 0 : bonusDel.hashCode) +
    (bonusAddLabel == null ? 0 : bonusAddLabel.hashCode) +
    (bonusDelLabel == null ? 0 : bonusDelLabel.hashCode);

  @override
  String toString() => 'Product[id=$id, externalId=$externalId, name=$name, type=$type, isBonus=$isBonus, bonusAdd=$bonusAdd, bonusDel=$bonusDel, bonusAddLabel=$bonusAddLabel, bonusDelLabel=$bonusDelLabel]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (id != null) {
      json[r'id'] = id;
    }
    if (externalId != null) {
      json[r'externalId'] = externalId;
    }
    if (name != null) {
      json[r'name'] = name;
    }
    if (type != null) {
      json[r'type'] = type;
    }
    if (isBonus != null) {
      json[r'isBonus'] = isBonus;
    }
    if (bonusAdd != null) {
      json[r'bonusAdd'] = bonusAdd;
    }
    if (bonusDel != null) {
      json[r'bonusDel'] = bonusDel;
    }
    if (bonusAddLabel != null) {
      json[r'bonusAddLabel'] = bonusAddLabel;
    }
    if (bonusDelLabel != null) {
      json[r'bonusDelLabel'] = bonusDelLabel;
    }
    return json;
  }

  /// Returns a new [Product] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static Product fromJson(Map<String, dynamic> json) => json == null
    ? null
    : Product(
        id: json[r'id'],
        externalId: json[r'externalId'],
        name: json[r'name'],
        type: ProductType.fromJson(json[r'type']),
        isBonus: json[r'isBonus'],
        bonusAdd: json[r'bonusAdd'] == null
          ? null
          : (json[r'bonusAdd'] as List).cast<num>(),
        bonusDel: json[r'bonusDel'] == null
          ? null
          : (json[r'bonusDel'] as List).cast<num>(),
        bonusAddLabel: json[r'bonusAddLabel'],
        bonusDelLabel: json[r'bonusDelLabel'],
    );

  static List<Product> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <Product>[]
      : json.map((v) => Product.fromJson(v)).toList(growable: true == growable);

  static Map<String, Product> mapFromJson(Map<String, dynamic> json) {
    final map = <String, Product>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = Product.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of Product-objects as value to a dart map
  static Map<String, List<Product>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<Product>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = Product.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        String id,

        String externalId,

        String name,

        ProductType type,

        bool isBonus,

        List<num> bonusAdd,

        List<num> bonusDel,

        String bonusAddLabel,

        String bonusDelLabel,

    }) {
    return Product(
        id: id ?? this.id,
        externalId: externalId ?? this.externalId,
        name: name ?? this.name,
        type: type ?? this.type,
        isBonus: isBonus ?? this.isBonus,
        bonusAdd: bonusAdd ?? this.bonusAdd,
        bonusDel: bonusDel ?? this.bonusDel,
        bonusAddLabel: bonusAddLabel ?? this.bonusAddLabel,
        bonusDelLabel: bonusDelLabel ?? this.bonusDelLabel,
    );
    }


    }

