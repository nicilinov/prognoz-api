//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;

class InlineResponse20012 {
  /// Returns a new [InlineResponse20012] instance.
  InlineResponse20012({
    this.data = const [],
  });

  List<Product> data;

  @override
  bool operator ==(Object other) => identical(this, other) || other is InlineResponse20012 &&
     other.data == data;

  @override
  int get hashCode =>
    (data == null ? 0 : data.hashCode);

  @override
  String toString() => 'InlineResponse20012[data=$data]';

  Map<String, dynamic> toJson() {
    final json = <String, dynamic>{};
    if (data != null) {
      json[r'data'] = data;
    }
    return json;
  }

  /// Returns a new [InlineResponse20012] instance and imports its values from
  /// [json] if it's non-null, null if [json] is null.
  static InlineResponse20012 fromJson(Map<String, dynamic> json) => json == null
    ? null
    : InlineResponse20012(
        data: Product.listFromJson(json[r'data']),
    );

  static List<InlineResponse20012> listFromJson(List<dynamic> json, {bool emptyIsNull, bool growable,}) =>
    json == null || json.isEmpty
      ? true == emptyIsNull ? null : <InlineResponse20012>[]
      : json.map((v) => InlineResponse20012.fromJson(v)).toList(growable: true == growable);

  static Map<String, InlineResponse20012> mapFromJson(Map<String, dynamic> json) {
    final map = <String, InlineResponse20012>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) => map[key] = InlineResponse20012.fromJson(v));
    }
    return map;
  }

  // maps a json object with a list of InlineResponse20012-objects as value to a dart map
  static Map<String, List<InlineResponse20012>> mapListFromJson(Map<String, dynamic> json, {bool emptyIsNull, bool growable,}) {
    final map = <String, List<InlineResponse20012>>{};
    if (json != null && json.isNotEmpty) {
      json.forEach((String key, dynamic v) {
        map[key] = InlineResponse20012.listFromJson(v, emptyIsNull: emptyIsNull, growable: growable);
      });
    }
    return map;
  }

    copyWith({


        List<Product> data,

    }) {
    return InlineResponse20012(
        data: data ?? this.data,
    );
    }


    }

