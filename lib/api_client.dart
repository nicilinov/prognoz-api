//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


typedef OnPreSend(
String path,
String method,
Iterable<QueryParam> queryParams,
Object body,
Map<String, String> headerParams,
Map<String, String> formParams,
String nullableContentType,
List<String> authNames,
);

typedef Future<Map<String, String>> GetAuthToken();

class ApiClient {
  ApiClient({this.basePath = 'http://cl-b-test.ai.prognoz.me/clientapi/v1', this.onPreSend, this.getAuthToken,}) {
    // Setup authentications (key: authentication name, value: authentication).
    _authentications[r'JWT'] = ApiKeyAuth('header', 'Authorization');
  }

  final String basePath;
  final OnPreSend onPreSend;
  final GetAuthToken getAuthToken;

  var _client = Dio();

  /// Returns the current HTTP [Client] instance to use in this class.
  ///
  /// The return value is guaranteed to never be null.
  Dio get client => _client;

  /// Requests to use a new HTTP [Client] in this class.
  ///
  /// If the [newClient] is null, an [ArgumentError] is thrown.
  set client(Dio newClient) {
    if (newClient == null) {
      throw ArgumentError('New client instance cannot be null.');
    }
    _client = newClient;
  }

  final _defaultHeaderMap = <String, String>{};
  final _authentications = <String, Authentication>{};

  void addDefaultHeader(String key, String value) {
     _defaultHeaderMap[key] = value;
  }

  dynamic deserialize(dynamic json, String targetType, {bool growable}) {
    // Remove all spaces.  Necessary for reg expressions as well.
    targetType = targetType.replaceAll(' ', '');

    return targetType == 'String'
      ? json
        : _deserialize(json, targetType, growable: true == growable);
  }

  String serialize(Object obj) => obj == null ? '' : json.encode(obj);

  T getAuthentication<T extends Authentication>(String name) {
    final authentication = _authentications[name];
    return authentication is T ? authentication : null;
  }

  // We don’t use a Map<String, String> for queryParams.
  // If collectionFormat is 'multi', a key might appear multiple times.
  Future<Response> invokeAPI(
    String path,
    String method,
    Iterable<QueryParam> queryParams,
    Object body,
    Map<String, String> headerParams,
    Map<String, String> formParams,
    String nullableContentType,
    List<String> authNames,
  ) async {
    _updateParamsForAuth(authNames, queryParams, headerParams);

    headerParams.addAll(_defaultHeaderMap);

    if (getAuthToken != null)
      headerParams.addAll(await getAuthToken());

                        var _queryParams = queryParams
                        .where((param) => param.value != null);

                        List<String> queryString = [];

                            if (_queryParams.isNotEmpty) {
                            queryString = [];
                            Map<String, int> _queryParamsKeys = {};

                            _queryParams.forEach((element) {
                            _queryParamsKeys["${element.name}"] = _queryParams
                            .where((item) => item.name == element.name)
                            .length;
                            });

                            _queryParams.forEach((element) {
                            String name = element.name;
                            if (_queryParamsKeys["$name"] > 1 && !name.contains('[]'))
                            name = "$name[]";
                            queryString.add("$name=${element.value}");
                            });
                            }

                            final url = '$basePath$path${queryString.isNotEmpty ? '?' + queryString.join('&') : ''}';


                            if (nullableContentType != null) {
                            headerParams['Content-Type'] = nullableContentType;
                            }

    try {
      // TODO отключил multipart

      // Special case for uploading a single file which isn’t a 'multipart/form-data'.
      /*if (
        body is MultipartFile && (nullableContentType == null ||
        !nullableContentType.toLowerCase().startsWith('multipart/form-data'))
      ) {
        final request = StreamedRequest(method, Uri.parse(url));
        request.headers.addAll(headerParams);
        request.contentLength = body.length;
        body.finalize().listen(
          request.sink.add,
          onDone: request.sink.close,
          onError: (error, trace) => request.sink.close(),
          cancelOnError: true,
        );
        final response = await _client.send(request);
        return Response.fromStream(response);
      }

      if (body is MultipartRequest) {
        final request = MultipartRequest(method, Uri.parse(url));
        request.fields.addAll(body.fields);
        request.files.addAll(body.files);
        request.headers.addAll(body.headers);
        request.headers.addAll(headerParams);
        final response = await _client.send(request);
        return Response.fromStream(response);
      }*/

      final msgBody = nullableContentType == 'application/x-www-form-urlencoded'
        ? formParams
        : serialize(body);
      final nullableHeaderParams = headerParams.isEmpty ? null : headerParams;

    if (onPreSend != null)
        onPreSend(
            path,
            method,
            queryParams,
            body,
            headerParams,
            formParams,
            nullableContentType,
            authNames,
        );

      switch(method) {
        case 'POST': return await _client.post(url, options: Options(headers: nullableHeaderParams), data: msgBody,);
        case 'PUT': return await _client.put(url, options: Options(headers: nullableHeaderParams), data: msgBody,);
        case 'DELETE': return await _client.delete(url, options: Options(headers: nullableHeaderParams), data: msgBody,);
        case 'PATCH': return await _client.patch(url, options: Options(headers: nullableHeaderParams), data: msgBody,);
        case 'HEAD': return await _client.head(url, options: Options(headers: nullableHeaderParams), data: msgBody,);
        case 'GET': return await _client.get(url, options: Options(headers: nullableHeaderParams),);
      }
    } on SocketException catch (e, trace) {
      throw ApiException.withInner(HttpStatus.badRequest, 'Socket operation failed: $method $path', e, trace,);
    } on TlsException catch (e, trace) {
      throw ApiException.withInner(HttpStatus.badRequest, 'TLS/SSL communication failed: $method $path', e, trace,);
    } on IOException catch (e, trace) {
      throw ApiException.withInner(HttpStatus.badRequest, 'I/O operation failed: $method $path', e, trace,);
    } on DioError catch (e, trace) {
      throw ApiException.withInner(HttpStatus.badRequest, 'HTTP connection failed: $method $path', e, trace,);
    } on Exception catch (e, trace) {
      throw ApiException.withInner(HttpStatus.badRequest, 'Exception occurred: $method $path', e, trace,);
    }

    throw ApiException(HttpStatus.badRequest, 'Invalid HTTP operation: $method $path',);
  }

  dynamic _deserialize(dynamic value, String targetType, {bool growable}) {
    try {
      switch (targetType) {
        case 'String':
          return '$value';
        case 'int':
          return value is int ? value : int.parse('$value');
        case 'bool':
          if (value is bool) {
            return value;
          }
          final valueString = '$value'.toLowerCase();
          return valueString == 'true' || valueString == '1';
          break;
        case 'double':
          return value is double ? value : double.parse('$value');
        case 'ClientStatus':
          return ClientStatus.fromJson(value);
        case 'ContentArticle':
          return ContentArticle.fromJson(value);
        case 'ContentButton':
          return ContentButton.fromJson(value);
        case 'ContentSection':
          return ContentSection.fromJson(value);
        case 'ContentSubSection':
          return ContentSubSection.fromJson(value);
        case 'Coupon':
          return Coupon.fromJson(value);
        case 'DeviceSettings':
          return DeviceSettings.fromJson(value);
        case 'InlineResponse200':
          return InlineResponse200.fromJson(value);
        case 'InlineResponse2001':
          return InlineResponse2001.fromJson(value);
        case 'InlineResponse20010':
          return InlineResponse20010.fromJson(value);
        case 'InlineResponse20011':
          return InlineResponse20011.fromJson(value);
        case 'InlineResponse20012':
          return InlineResponse20012.fromJson(value);
        case 'InlineResponse20013':
          return InlineResponse20013.fromJson(value);
        case 'InlineResponse20014':
          return InlineResponse20014.fromJson(value);
        case 'InlineResponse2001Data':
          return InlineResponse2001Data.fromJson(value);
        case 'InlineResponse2002':
          return InlineResponse2002.fromJson(value);
        case 'InlineResponse2003':
          return InlineResponse2003.fromJson(value);
        case 'InlineResponse2003Data':
          return InlineResponse2003Data.fromJson(value);
        case 'InlineResponse2004':
          return InlineResponse2004.fromJson(value);
        case 'InlineResponse2005':
          return InlineResponse2005.fromJson(value);
        case 'InlineResponse2006':
          return InlineResponse2006.fromJson(value);
        case 'InlineResponse2007':
          return InlineResponse2007.fromJson(value);
        case 'InlineResponse2007Data':
          return InlineResponse2007Data.fromJson(value);
        case 'InlineResponse2008':
          return InlineResponse2008.fromJson(value);
        case 'InlineResponse2009':
          return InlineResponse2009.fromJson(value);
        case 'InlineResponse200Data':
          return InlineResponse200Data.fromJson(value);
        case 'MessageSubject':
          return MessageSubject.fromJson(value);
        case 'MetaPagination':
          return MetaPagination.fromJson(value);
        case 'ModelClient':
          return ModelClient.fromJson(value);
        case 'NotificationDBO':
          return NotificationDBO.fromJson(value);
        case 'NotificationType':
          return NotificationType.fromJson(value);
        case 'Place':
          return Place.fromJson(value);
        case 'PlaceService':
          return PlaceService.fromJson(value);
        case 'Product':
          return Product.fromJson(value);
        case 'ProductType':
          return ProductType.fromJson(value);
        default:
          Match match;
          if (value is List && (match = _regList.firstMatch(targetType)) != null) {
            final newTargetType = match[1];
            return value
              .map((v) => _deserialize(v, newTargetType, growable: growable))
              .toList(growable: true == growable);
          }
          if (value is Map && (match = _regMap.firstMatch(targetType)) != null) {
            final newTargetType = match[1];
            return Map.fromIterables(
              value.keys,
              value.values.map((v) => _deserialize(v, newTargetType, growable: growable)),
            );
          }
          break;
      }
    } on Exception catch (e, stack) {
      throw ApiException.withInner(HttpStatus.internalServerError, 'Exception during deserialization.', e, stack,);
    }
    throw ApiException(HttpStatus.internalServerError, 'Could not find a suitable class for deserialization',);
  }

  /// Update query and header parameters based on authentication settings.
  /// @param authNames The authentications to apply
  void _updateParamsForAuth(
    List<String> authNames,
    List<QueryParam> queryParams,
    Map<String, String> headerParams,
  ) {
    authNames.forEach((authName) {
      final auth = _authentications[authName];
      if (auth == null) {
        throw ArgumentError('Authentication undefined: $authName');
      }
      auth.applyToParams(queryParams, headerParams);
    });
  }
}
