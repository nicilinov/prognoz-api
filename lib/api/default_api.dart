//
// AUTO-GENERATED FILE, DO NOT MODIFY!
//
// @dart=2.0

// ignore_for_file: unused_element, unused_import
// ignore_for_file: always_put_required_named_parameters_first
// ignore_for_file: lines_longer_than_80_chars

part of openapi.api;


class DefaultApi {
  DefaultApi([ApiClient apiClient]) : apiClient = apiClient ?? defaultApiClient;

  final ApiClient apiClient;

  /// Добавление сообщения клиента
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [List<String>] subjects (required):
  ///   Код темы обращения
  ///
  /// * [String] email (required):
  ///   E-mail
  ///
  /// * [String] fio (required):
  ///   Имя
  ///
  /// * [String] description (required):
  ///   Текст обращения
  Future<Response> addMessageWithHttpInfo(
        List<String> subjects, 
        String email, 
        String fio, 
        String description, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (subjects == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: subjects');
    }
    if (email == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: email');
    }
    if (fio == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: fio');
    }
    if (description == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: description');
    }

    final path = '/messages'.replaceAll('{format}', 'json');

    Object postBody = {
         'subjects': subjects,  'email': email,  'fio': fio,  'description': description, 
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['multipart/form-data'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (subjects != null) {
        hasFields = true;
        mp.fields[r'subjects'] = parameterToString(subjects);
      }
      if (email != null) {
        hasFields = true;
        mp.fields[r'email'] = parameterToString(email);
      }
      if (fio != null) {
        hasFields = true;
        mp.fields[r'fio'] = parameterToString(fio);
      }
      if (description != null) {
        hasFields = true;
        mp.fields[r'description'] = parameterToString(description);
      }
      if (hasFields) {
        postBody = mp;
      }
    } else {
      if (subjects != null) {
        formParams[r'subjects'] = parameterToString(subjects);
      }
      if (email != null) {
        formParams[r'email'] = parameterToString(email);
      }
      if (fio != null) {
        formParams[r'fio'] = parameterToString(fio);
      }
      if (description != null) {
        formParams[r'description'] = parameterToString(description);
      }
    }*/

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Добавление сообщения клиента
  ///
  /// Parameters:
  ///
  /// * [List<String>] subjects (required):
  ///   Код темы обращения
  ///
  /// * [String] email (required):
  ///   E-mail
  ///
  /// * [String] fio (required):
  ///   Имя
  ///
  /// * [String] description (required):
  ///   Текст обращения
      Future<InlineResponse2007> addMessage(
          List<String> subjects, String email, String fio, String description, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await addMessageWithHttpInfo(
        subjects, email, fio, description, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2007') as InlineResponse2007;
    }
    return null;
  }

  /// Запрос одноразового пароля по номеру телефона
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] phoneNumber (required):
  ///   Номер телефона клиента в формате 79139050305
  Future<Response> authWithHttpInfo(
        String phoneNumber, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (phoneNumber == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: phoneNumber');
    }

    final path = '/auth'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'phoneNumber', phoneNumber));

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Запрос одноразового пароля по номеру телефона
  ///
  /// Parameters:
  ///
  /// * [String] phoneNumber (required):
  ///   Номер телефона клиента в формате 79139050305
      Future<InlineResponse200> auth(
          String phoneNumber, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await authWithHttpInfo(
        phoneNumber, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse200') as InlineResponse200;
    }
    return null;
  }

  /// Список персональных купонов клиента
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> clientCouponsWithHttpInfo(
          {
              Map<String, dynamic> extra,
          }
  ) async {
    final path = '/me/coupons'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Список персональных купонов клиента
      Future<InlineResponse2006> clientCoupons(
          
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await clientCouponsWithHttpInfo(
        
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2006') as InlineResponse2006;
    }
    return null;
  }

  /// Отправка уведомлений о купонах при приближении к ближайшей АЗС
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [List<String>] placeIds (required):
  ///   Id ближайших заправок
  Future<Response> clientCouponsRemindWithHttpInfo(
        List<String> placeIds, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (placeIds == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: placeIds');
    }

    final path = '/me/remind/coupons'.replaceAll('{format}', 'json');

    Object postBody = {
         'placeIds': placeIds, 
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['multipart/form-data'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (placeIds != null) {
        hasFields = true;
        mp.fields[r'placeIds'] = parameterToString(placeIds);
      }
      if (hasFields) {
        postBody = mp;
      }
    } else {
      if (placeIds != null) {
        formParams[r'placeIds'] = parameterToString(placeIds);
      }
    }*/

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Отправка уведомлений о купонах при приближении к ближайшей АЗС
  ///
  /// Parameters:
  ///
  /// * [List<String>] placeIds (required):
  ///   Id ближайших заправок
      Future<InlineResponse2007> clientCouponsRemind(
          List<String> placeIds, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await clientCouponsRemindWithHttpInfo(
        placeIds, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2007') as InlineResponse2007;
    }
    return null;
  }

  /// Получение информации о клиенте
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> clientInfoWithHttpInfo(
          {
              Map<String, dynamic> extra,
          }
  ) async {
    final path = '/me'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Получение информации о клиенте
      Future<InlineResponse2002> clientInfo(
          
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await clientInfoWithHttpInfo(
        
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2002') as InlineResponse2002;
    }
    return null;
  }

  /// Получение временного QR-кода клиента
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> clientQrCodeWithHttpInfo(
          {
              Map<String, dynamic> extra,
          }
  ) async {
    final path = '/me/qrcode'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Получение временного QR-кода клиента
      Future<InlineResponse2003> clientQrCode(
          
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await clientQrCodeWithHttpInfo(
        
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2003') as InlineResponse2003;
    }
    return null;
  }

  /// Проверка одноразового пароля авторизации/регистрации
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] phoneNumber (required):
  ///   Номер телефона клиента в формате 79139050305
  ///
  /// * [String] code (required):
  ///   Одноразовый пароль клиента из СМС
  ///
  /// * [String] deviceToken (required):
  ///   Device token устройства
  Future<Response> completeAuthWithHttpInfo(
        String phoneNumber, 
        String code, 
        String deviceToken, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (phoneNumber == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: phoneNumber');
    }
    if (code == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: code');
    }
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }

    final path = '/complete_auth'.replaceAll('{format}', 'json');

    Object postBody = {
         'phoneNumber': phoneNumber,  'code': code,  'deviceToken': deviceToken, 
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['multipart/form-data'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (phoneNumber != null) {
        hasFields = true;
        mp.fields[r'phoneNumber'] = parameterToString(phoneNumber);
      }
      if (code != null) {
        hasFields = true;
        mp.fields[r'code'] = parameterToString(code);
      }
      if (deviceToken != null) {
        hasFields = true;
        mp.fields[r'deviceToken'] = parameterToString(deviceToken);
      }
      if (hasFields) {
        postBody = mp;
      }
    } else {
      if (phoneNumber != null) {
        formParams[r'phoneNumber'] = parameterToString(phoneNumber);
      }
      if (code != null) {
        formParams[r'code'] = parameterToString(code);
      }
      if (deviceToken != null) {
        formParams[r'deviceToken'] = parameterToString(deviceToken);
      }
    }*/

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Проверка одноразового пароля авторизации/регистрации
  ///
  /// Parameters:
  ///
  /// * [String] phoneNumber (required):
  ///   Номер телефона клиента в формате 79139050305
  ///
  /// * [String] code (required):
  ///   Одноразовый пароль клиента из СМС
  ///
  /// * [String] deviceToken (required):
  ///   Device token устройства
      Future<InlineResponse2001> completeAuth(
          String phoneNumber, String code, String deviceToken, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await completeAuthWithHttpInfo(
        phoneNumber, code, deviceToken, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2001') as InlineResponse2001;
    }
    return null;
  }

  /// Получение информации о настройках устройства
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceUdid (required):
  ///   Udid устройства
  ///
  /// * [String] deviceToken (required):
  ///   Device token устройства
  ///
  /// * [String] deviceType (required):
  ///   Тип устройства
  Future<Response> deviceSettingsListWithHttpInfo(
        String deviceUdid, 
        String deviceToken, 
        String deviceType, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (deviceUdid == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceUdid');
    }
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }
    if (deviceType == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceType');
    }

    final path = '/device/settings/list'.replaceAll('{format}', 'json');

    Object postBody = {
         'deviceUdid': deviceUdid,  'deviceToken': deviceToken,  'deviceType': deviceType, 
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['multipart/form-data'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (deviceUdid != null) {
        hasFields = true;
        mp.fields[r'deviceUdid'] = parameterToString(deviceUdid);
      }
      if (deviceToken != null) {
        hasFields = true;
        mp.fields[r'deviceToken'] = parameterToString(deviceToken);
      }
      if (deviceType != null) {
        hasFields = true;
        mp.fields[r'deviceType'] = parameterToString(deviceType);
      }
      if (hasFields) {
        postBody = mp;
      }
    } else {
      if (deviceUdid != null) {
        formParams[r'deviceUdid'] = parameterToString(deviceUdid);
      }
      if (deviceToken != null) {
        formParams[r'deviceToken'] = parameterToString(deviceToken);
      }
      if (deviceType != null) {
        formParams[r'deviceType'] = parameterToString(deviceType);
      }
    }*/

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Получение информации о настройках устройства
  ///
  /// Parameters:
  ///
  /// * [String] deviceUdid (required):
  ///   Udid устройства
  ///
  /// * [String] deviceToken (required):
  ///   Device token устройства
  ///
  /// * [String] deviceType (required):
  ///   Тип устройства
      Future<InlineResponse2004> deviceSettingsList(
          String deviceUdid, String deviceToken, String deviceType, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await deviceSettingsListWithHttpInfo(
        deviceUdid, deviceToken, deviceType, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2004') as InlineResponse2004;
    }
    return null;
  }

  /// Сохранение информации о настройках устройства
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Device token устройства
  ///
  /// * [int] geoEnabled:
  ///   Геолокация включена (0 либо 1)
  ///
  /// * [int] notificationsEnabled:
  ///   Все уведомления включены (0 либо 1)
  ///
  /// * [int] adEnabled:
  ///   Все рекламные акции включены (0 либо 1)
  ///
  /// * [int] personalOffersEnabled:
  ///   Индивидуальные предложения включены (0 либо 1)
  ///
  /// * [int] techInfoEnabled:
  ///   Техническая информация включена (0 либо 1)
  Future<Response> deviceSettingsSaveWithHttpInfo(
        String deviceToken, 
        
        
        
        
        
            {
                int geoEnabled,int notificationsEnabled,int adEnabled,int personalOffersEnabled,int techInfoEnabled,
            Map<String, dynamic> extra,
            }
  ) async {
    // Verify required params are set.
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }

    final path = '/device/settings/save'.replaceAll('{format}', 'json');

    Object postBody = {
         'deviceToken': deviceToken,  'geoEnabled': geoEnabled,  'notificationsEnabled': notificationsEnabled,  'adEnabled': adEnabled,  'personalOffersEnabled': personalOffersEnabled,  'techInfoEnabled': techInfoEnabled, 
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['multipart/form-data'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (deviceToken != null) {
        hasFields = true;
        mp.fields[r'deviceToken'] = parameterToString(deviceToken);
      }
      if (geoEnabled != null) {
        hasFields = true;
        mp.fields[r'geoEnabled'] = parameterToString(geoEnabled);
      }
      if (notificationsEnabled != null) {
        hasFields = true;
        mp.fields[r'notificationsEnabled'] = parameterToString(notificationsEnabled);
      }
      if (adEnabled != null) {
        hasFields = true;
        mp.fields[r'adEnabled'] = parameterToString(adEnabled);
      }
      if (personalOffersEnabled != null) {
        hasFields = true;
        mp.fields[r'personalOffersEnabled'] = parameterToString(personalOffersEnabled);
      }
      if (techInfoEnabled != null) {
        hasFields = true;
        mp.fields[r'techInfoEnabled'] = parameterToString(techInfoEnabled);
      }
      if (hasFields) {
        postBody = mp;
      }
    } else {
      if (deviceToken != null) {
        formParams[r'deviceToken'] = parameterToString(deviceToken);
      }
      if (geoEnabled != null) {
        formParams[r'geoEnabled'] = parameterToString(geoEnabled);
      }
      if (notificationsEnabled != null) {
        formParams[r'notificationsEnabled'] = parameterToString(notificationsEnabled);
      }
      if (adEnabled != null) {
        formParams[r'adEnabled'] = parameterToString(adEnabled);
      }
      if (personalOffersEnabled != null) {
        formParams[r'personalOffersEnabled'] = parameterToString(personalOffersEnabled);
      }
      if (techInfoEnabled != null) {
        formParams[r'techInfoEnabled'] = parameterToString(techInfoEnabled);
      }
    }*/

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Сохранение информации о настройках устройства
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Device token устройства
  ///
  /// * [int] geoEnabled:
  ///   Геолокация включена (0 либо 1)
  ///
  /// * [int] notificationsEnabled:
  ///   Все уведомления включены (0 либо 1)
  ///
  /// * [int] adEnabled:
  ///   Все рекламные акции включены (0 либо 1)
  ///
  /// * [int] personalOffersEnabled:
  ///   Индивидуальные предложения включены (0 либо 1)
  ///
  /// * [int] techInfoEnabled:
  ///   Техническая информация включена (0 либо 1)
      Future<InlineResponse2004> deviceSettingsSave(
          String deviceToken, 
                  {
                    int geoEnabled,int notificationsEnabled,int adEnabled,int personalOffersEnabled,int techInfoEnabled,
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await deviceSettingsSaveWithHttpInfo(
        deviceToken, 
                
                geoEnabled: geoEnabled, 
                notificationsEnabled: notificationsEnabled, 
                adEnabled: adEnabled, 
                personalOffersEnabled: personalOffersEnabled, 
                techInfoEnabled: techInfoEnabled, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2004') as InlineResponse2004;
    }
    return null;
  }

  /// Получение раздела со статьями
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] code (required):
  ///   Код раздела
  ///
  /// * [int] page:
  ///   Пагинация для статей, входящих в раздел
  ///
  /// * [int] perPage:
  ///   Статей раздела на страницу
  Future<Response> getContentSectionWithHttpInfo(
        String code, 
        
        
            {
                int page,int perPage,
            Map<String, dynamic> extra,
            }
  ) async {
    // Verify required params are set.
    if (code == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: code');
    }

    final path = '/content/{code}'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'code' + '}', code.toString());

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (page != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'page', page));
    }
    if (perPage != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'perPage', perPage));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Получение раздела со статьями
  ///
  /// Parameters:
  ///
  /// * [String] code (required):
  ///   Код раздела
  ///
  /// * [int] page:
  ///   Пагинация для статей, входящих в раздел
  ///
  /// * [int] perPage:
  ///   Статей раздела на страницу
      Future<InlineResponse20013> getContentSection(
          String code, 
                  {
                    int page,int perPage,
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getContentSectionWithHttpInfo(
        code, 
                
                page: page, 
                perPage: perPage, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse20013') as InlineResponse20013;
    }
    return null;
  }

  /// Получение купона
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [int] id (required):
  ///   Id купона
  Future<Response> getCouponWithHttpInfo(
        int id, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }

    final path = '/coupons/{id}'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Получение купона
  ///
  /// Parameters:
  ///
  /// * [int] id (required):
  ///   Id купона
      Future<InlineResponse2009> getCoupon(
          int id, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getCouponWithHttpInfo(
        id, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2009') as InlineResponse2009;
    }
    return null;
  }

  /// Список общих купонов
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [int] page:
  ///   Пагинация
  ///
  /// * [int] perPage:
  ///   Записей на страницу
  Future<Response> getCouponsWithHttpInfo(
        
        
            {
                int page,int perPage,
            Map<String, dynamic> extra,
            }
  ) async {
    // Verify required params are set.

    final path = '/coupons'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (page != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'page', page));
    }
    if (perPage != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'perPage', perPage));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Список общих купонов
  ///
  /// Parameters:
  ///
  /// * [int] page:
  ///   Пагинация
  ///
  /// * [int] perPage:
  ///   Записей на страницу
      Future<InlineResponse2005> getCoupons(
          
                  {
                    int page,int perPage,
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getCouponsWithHttpInfo(
        
                page: page, 
                perPage: perPage, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2005') as InlineResponse2005;
    }
    return null;
  }

  /// Получение списка тем сообщения
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> getMessageSubjectsWithHttpInfo(
          {
              Map<String, dynamic> extra,
          }
  ) async {
    final path = '/messages/subjects'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Получение списка тем сообщения
      Future<InlineResponse20014> getMessageSubjects(
          
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getMessageSubjectsWithHttpInfo(
        
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse20014') as InlineResponse20014;
    }
    return null;
  }

  /// Список сообщений пользователя
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
  ///
  /// * [int] page:
  ///   Пагинация для сообщений
  ///
  /// * [int] perPage:
  ///   Сообщений на страницу
  ///
  /// * [String] search:
  ///   Поисковый запрос
  Future<Response> getNotificationsWithHttpInfo(
        String deviceToken, 
        
        
        
            {
                int page,int perPage,String search,
            Map<String, dynamic> extra,
            }
  ) async {
    // Verify required params are set.
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }

    final path = '/me/notifications'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (page != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'page', page));
    }
    if (perPage != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'perPage', perPage));
    }
    if (search != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'search', search));
    }
      queryParams.addAll(_convertParametersForCollectionFormat('', 'deviceToken', deviceToken));

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Список сообщений пользователя
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
  ///
  /// * [int] page:
  ///   Пагинация для сообщений
  ///
  /// * [int] perPage:
  ///   Сообщений на страницу
  ///
  /// * [String] search:
  ///   Поисковый запрос
      Future<InlineResponse2008> getNotifications(
          String deviceToken, 
                  {
                    int page,int perPage,String search,
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getNotificationsWithHttpInfo(
        deviceToken, 
                
                page: page, 
                perPage: perPage, 
                search: search, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2008') as InlineResponse2008;
    }
    return null;
  }

  /// Список услуг торговых точек
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> getPlaceServicesWithHttpInfo(
          {
              Map<String, dynamic> extra,
          }
  ) async {
    final path = '/place_services'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Список услуг торговых точек
      Future<InlineResponse20011> getPlaceServices(
          
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getPlaceServicesWithHttpInfo(
        
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse20011') as InlineResponse20011;
    }
    return null;
  }

  /// Список торговых точек
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [List<String>] productIds:
  ///   Фильтр торговых точек по товарам (топливу)
  ///
  /// * [List<String>] serviceIds:
  ///   Фильтр торговых точек по видам услуг
  ///
  /// * [String] search:
  ///   Поисковый запрос
  Future<Response> getPlacesWithHttpInfo(
        
        
        
            {
                List<String> productIds,List<String> serviceIds,String search,
            Map<String, dynamic> extra,
            }
  ) async {
    // Verify required params are set.

    final path = '/places'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (productIds != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('csv', 'productIds', productIds));
    }
    if (serviceIds != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('csv', 'serviceIds', serviceIds));
    }
    if (search != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'search', search));
    }

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Список торговых точек
  ///
  /// Parameters:
  ///
  /// * [List<String>] productIds:
  ///   Фильтр торговых точек по товарам (топливу)
  ///
  /// * [List<String>] serviceIds:
  ///   Фильтр торговых точек по видам услуг
  ///
  /// * [String] search:
  ///   Поисковый запрос
      Future<InlineResponse20010> getPlaces(
          
                  {
                    List<String> productIds,List<String> serviceIds,String search,
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getPlacesWithHttpInfo(
        
                productIds: productIds, 
                serviceIds: serviceIds, 
                search: search, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse20010') as InlineResponse20010;
    }
    return null;
  }

  /// Список товаров (видов топлива)
  ///
  /// Note: This method returns the HTTP [Response].
  Future<Response> getProductsWithHttpInfo(
          {
              Map<String, dynamic> extra,
          }
  ) async {
    final path = '/products'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>[];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Список товаров (видов топлива)
      Future<InlineResponse20012> getProducts(
          
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await getProductsWithHttpInfo(
        
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse20012') as InlineResponse20012;
    }
    return null;
  }

  /// Удаление всех сообщений
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
  ///
  /// * [int] page:
  ///   Пагинация для сообщений
  ///
  /// * [int] perPage:
  ///   Сообщений на страницу
  ///
  /// * [String] search:
  ///   Поисковый запрос
  Future<Response> notificationsDeleteAllWithHttpInfo(
        String deviceToken, 
        
        
        
            {
                int page,int perPage,String search,
            Map<String, dynamic> extra,
            }
  ) async {
    // Verify required params are set.
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }

    final path = '/me/notifications'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    if (page != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'page', page));
    }
    if (perPage != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'perPage', perPage));
    }
    if (search != null) {
      queryParams.addAll(_convertParametersForCollectionFormat('', 'search', search));
    }
      queryParams.addAll(_convertParametersForCollectionFormat('', 'deviceToken', deviceToken));

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'DELETE',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Удаление всех сообщений
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
  ///
  /// * [int] page:
  ///   Пагинация для сообщений
  ///
  /// * [int] perPage:
  ///   Сообщений на страницу
  ///
  /// * [String] search:
  ///   Поисковый запрос
      Future<InlineResponse2007> notificationsDeleteAll(
          String deviceToken, 
                  {
                    int page,int perPage,String search,
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await notificationsDeleteAllWithHttpInfo(
        deviceToken, 
                
                page: page, 
                perPage: perPage, 
                search: search, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2007') as InlineResponse2007;
    }
    return null;
  }

  /// Удаление одного сообщения
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///   Id сообщения
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
  Future<Response> notificationsDeleteOneWithHttpInfo(
        String id, 
        String deviceToken, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }

    final path = '/me/notifications/{id}'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'deviceToken', deviceToken));

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'DELETE',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Удаление одного сообщения
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///   Id сообщения
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
      Future<InlineResponse2007> notificationsDeleteOne(
          String id, String deviceToken, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await notificationsDeleteOneWithHttpInfo(
        id, deviceToken, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2007') as InlineResponse2007;
    }
    return null;
  }

  /// Прочтение всех сообщений
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
  Future<Response> notificationsReadAllWithHttpInfo(
        String deviceToken, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }

    final path = '/me/notifications/read_all'.replaceAll('{format}', 'json');

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'deviceToken', deviceToken));

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Прочтение всех сообщений
  ///
  /// Parameters:
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
      Future<InlineResponse2007> notificationsReadAll(
          String deviceToken, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await notificationsReadAllWithHttpInfo(
        deviceToken, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2007') as InlineResponse2007;
    }
    return null;
  }

  /// Прочтение одного сообщения
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///   Id сообщения
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
  Future<Response> notificationsReadOneWithHttpInfo(
        String id, 
        String deviceToken, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }
    if (deviceToken == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: deviceToken');
    }

    final path = '/me/notifications/{id}/read'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody = {
        
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

      queryParams.addAll(_convertParametersForCollectionFormat('', 'deviceToken', deviceToken));

    final contentTypes = <String>[];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (hasFields) {
        postBody = mp;
      }
    } else {
    }*/

    return await apiClient.invokeAPI(
      path,
      'GET',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Прочтение одного сообщения
  ///
  /// Parameters:
  ///
  /// * [String] id (required):
  ///   Id сообщения
  ///
  /// * [String] deviceToken (required):
  ///   Token устройства
      Future<InlineResponse2007> notificationsReadOne(
          String id, String deviceToken, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await notificationsReadOneWithHttpInfo(
        id, deviceToken, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2007') as InlineResponse2007;
    }
    return null;
  }

  /// Установка признака уведомления клиента о купоне при приближении к заправке
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [int] id (required):
  ///   Id купона
  ///
  /// * [int] remind (required):
  ///   Признак уведомления у купона. Отправлять 0 либо 1
  Future<Response> setCouponRemindWithHttpInfo(
        int id, 
        int remind, 
          {
              Map<String, dynamic> extra,
          }
  ) async {
    // Verify required params are set.
    if (id == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: id');
    }
    if (remind == null) {
     throw ApiException(HttpStatus.badRequest, 'Missing required param: remind');
    }

    final path = '/me/remind/coupons/{id}'.replaceAll('{format}', 'json')
      .replaceAll('{' + 'id' + '}', id.toString());

    Object postBody = {
         'remind': remind, 
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['multipart/form-data'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (remind != null) {
        hasFields = true;
        mp.fields[r'remind'] = parameterToString(remind);
      }
      if (hasFields) {
        postBody = mp;
      }
    } else {
      if (remind != null) {
        formParams[r'remind'] = parameterToString(remind);
      }
    }*/

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Установка признака уведомления клиента о купоне при приближении к заправке
  ///
  /// Parameters:
  ///
  /// * [int] id (required):
  ///   Id купона
  ///
  /// * [int] remind (required):
  ///   Признак уведомления у купона. Отправлять 0 либо 1
      Future<InlineResponse2007> setCouponRemind(
          int id, int remind, 
                  {
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await setCouponRemindWithHttpInfo(
        id, remind, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2007') as InlineResponse2007;
    }
    return null;
  }

  /// Обновление информации о клиенте
  ///
  /// Note: This method returns the HTTP [Response].
  ///
  /// Parameters:
  ///
  /// * [String] phoneNumber:
  ///   Номер телефона
  ///
  /// * [String] email:
  ///   Email
  ///
  /// * [String] city:
  ///   Город
  ///
  /// * [String] name:
  ///   Имя
  ///
  /// * [String] surname:
  ///   Фамилия
  ///
  /// * [String] favoriteFuelId:
  ///   Id любимого вида топлива
  ///
  /// * [String] VK:
  ///   ВКонтакте клиента
  ///
  /// * [String] facebook:
  ///   Facebook клиента
  Future<Response> updateClientInfoWithHttpInfo(
        
        
        
        
        
        
        
        
            {
                String phoneNumber,String email,String city,String name,String surname,String favoriteFuelId,String VK,String facebook,
            Map<String, dynamic> extra,
            }
  ) async {
    // Verify required params are set.

    final path = '/me'.replaceAll('{format}', 'json');

    Object postBody = {
         'phoneNumber': phoneNumber,  'email': email,  'city': city,  'name': name,  'surname': surname,  'favoriteFuelId': favoriteFuelId,  'VK': VK,  'Facebook': facebook, 
      };

    final queryParams = <QueryParam>[];
    final headerParams = <String, String>{};
    final formParams = <String, String>{};

    final contentTypes = <String>['multipart/form-data'];
    final nullableContentType = contentTypes.isNotEmpty ? contentTypes[0] : null;
    final authNames = <String>['JWT'];

    /*if (
      nullableContentType != null &&
      nullableContentType.toLowerCase().startsWith('multipart/form-data')
    ) {
      bool hasFields = false;
      final mp = MultipartRequest(null, null);
      if (phoneNumber != null) {
        hasFields = true;
        mp.fields[r'phoneNumber'] = parameterToString(phoneNumber);
      }
      if (email != null) {
        hasFields = true;
        mp.fields[r'email'] = parameterToString(email);
      }
      if (city != null) {
        hasFields = true;
        mp.fields[r'city'] = parameterToString(city);
      }
      if (name != null) {
        hasFields = true;
        mp.fields[r'name'] = parameterToString(name);
      }
      if (surname != null) {
        hasFields = true;
        mp.fields[r'surname'] = parameterToString(surname);
      }
      if (favoriteFuelId != null) {
        hasFields = true;
        mp.fields[r'favoriteFuelId'] = parameterToString(favoriteFuelId);
      }
      if (VK != null) {
        hasFields = true;
        mp.fields[r'VK'] = parameterToString(VK);
      }
      if (facebook != null) {
        hasFields = true;
        mp.fields[r'Facebook'] = parameterToString(facebook);
      }
      if (hasFields) {
        postBody = mp;
      }
    } else {
      if (phoneNumber != null) {
        formParams[r'phoneNumber'] = parameterToString(phoneNumber);
      }
      if (email != null) {
        formParams[r'email'] = parameterToString(email);
      }
      if (city != null) {
        formParams[r'city'] = parameterToString(city);
      }
      if (name != null) {
        formParams[r'name'] = parameterToString(name);
      }
      if (surname != null) {
        formParams[r'surname'] = parameterToString(surname);
      }
      if (favoriteFuelId != null) {
        formParams[r'favoriteFuelId'] = parameterToString(favoriteFuelId);
      }
      if (VK != null) {
        formParams[r'VK'] = parameterToString(VK);
      }
      if (facebook != null) {
        formParams[r'Facebook'] = parameterToString(facebook);
      }
    }*/

    return await apiClient.invokeAPI(
      path,
      'POST',
      queryParams,
      postBody,
      headerParams,
      formParams,
      nullableContentType,
      authNames,
    );
  }

  /// Обновление информации о клиенте
  ///
  /// Parameters:
  ///
  /// * [String] phoneNumber:
  ///   Номер телефона
  ///
  /// * [String] email:
  ///   Email
  ///
  /// * [String] city:
  ///   Город
  ///
  /// * [String] name:
  ///   Имя
  ///
  /// * [String] surname:
  ///   Фамилия
  ///
  /// * [String] favoriteFuelId:
  ///   Id любимого вида топлива
  ///
  /// * [String] VK:
  ///   ВКонтакте клиента
  ///
  /// * [String] facebook:
  ///   Facebook клиента
      Future<InlineResponse2002> updateClientInfo(
          
                  {
                    String phoneNumber,String email,String city,String name,String surname,String favoriteFuelId,String VK,String facebook,
                    Map<String, dynamic> extra,
                  }
      ) async {
    final response = await updateClientInfoWithHttpInfo(
        
                phoneNumber: phoneNumber, 
                email: email, 
                city: city, 
                name: name, 
                surname: surname, 
                favoriteFuelId: favoriteFuelId, 
                VK: VK, 
                facebook: facebook, 
          extra: extra,
      );
    if (response.statusCode >= HttpStatus.badRequest) {
      throw ApiException(response.statusCode, _decodeBodyBytes(response));
    }
    // When a remote server returns no body with a status of 204, we shall not decode it.
    // At the time of writing this, `dart:convert` will throw an "Unexpected end of input"
    // FormatException when trying to decode an empty string.
    if (response.data != null && response.statusCode != HttpStatus.noContent) {
      return apiClient.deserialize(response.data, 'InlineResponse2002') as InlineResponse2002;
    }
    return null;
  }
}
