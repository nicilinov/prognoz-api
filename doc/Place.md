# prognoz_api.model.Place

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Id торговой точки | [optional] 
**name** | **String** | Название торговой точки | [optional] 
**address** | **String** | Адрес торговой точки | [optional] 
**latitude** | **num** | Широта торговой точки | [optional] 
**longtitude** | **num** | Долгота торговой точки | [optional] 
**services** | [**List<PlaceService>**](PlaceService.md) |  | [optional] [default to const []]
**products** | [**List<Product>**](Product.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


