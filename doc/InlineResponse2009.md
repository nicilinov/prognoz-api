# prognoz_api.model.InlineResponse2009

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**CouponFull**](CouponFull.md) |  | [optional] 
**meta** | [**MetaPagination**](MetaPagination.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


