# prognoz_api.model.Product

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** |  | [optional] 
**externalId** | **String** | Номенклатурный номер товара | [optional] 
**name** | **String** | Название товара | [optional] 
**type** | [**ProductType**](ProductType.md) |  | [optional] 
**isBonus** | **bool** | Признак участия в бонусной программе | [optional] 
**bonusAdd** | **List<num>** | Коэффициенты накопления баллов | [optional] [default to const []]
**bonusDel** | **List<num>** | Коэффициенты списания баллов | [optional] [default to const []]
**bonusAddLabel** | **String** | Коэффициенты накопления баллов на просмотре товара | [optional] 
**bonusDelLabel** | **String** | Коэффициенты списания баллов на просмотре товара | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


