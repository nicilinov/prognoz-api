# prognoz_api.model.ModelClient

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**cardId** | **String** | Номер карты лояльности клиента  (только просмотр) | [optional] 
**phoneNumber** | **String** | Номер телефона клиента | [optional] 
**email** | **String** | E-mail клиента | [optional] 
**isEmailConfirmed** | **bool** | E-mail клиента подтверждён?  (только просмотр) | [optional] 
**city** | **String** | Город клиента | [optional] 
**status** | [**ClientStatus**](ClientStatus.md) |  | [optional] 
**fio** | **String** | Ф.И.О. клиента  (только просмотр) | [optional] 
**name** | **String** | Имя клиента | [optional] 
**surname** | **String** | Фамилия клиента | [optional] 
**balance** | **int** | Баланс клиента  (только просмотр) | [optional] 
**favoriteFuelId** | **String** | Id любимого вида топлива клиента | [optional] 
**favoriteFuelName** | **String** | Название любимого вид топлива клиента (только просмотр) | [optional] 
**createdAt** | **String** | Дата регистрации  (только просмотр) | [optional] 
**VK** | **String** | VK клиента | [optional] 
**facebook** | **String** | Facebook клиента | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


