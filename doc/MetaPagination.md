# prognoz_api.model.MetaPagination

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**currentPage** | **int** |  | [optional] 
**perPage** | **int** |  | [optional] 
**lastPage** | **int** |  | [optional] 
**total** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


