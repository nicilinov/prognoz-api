# prognoz_api.api.DefaultApi

## Load the API package
```dart
import 'package:prognoz_api/api.dart';
```

All URIs are relative to *http://cl-b-test.ai.prognoz.me/clientapi/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**addMessage**](DefaultApi.md#addMessage) | **POST** /messages | Добавление сообщения клиента
[**auth**](DefaultApi.md#auth) | **GET** /auth | Запрос одноразового пароля по номеру телефона
[**clientCoupons**](DefaultApi.md#clientCoupons) | **GET** /me/coupons | Список персональных купонов клиента
[**clientCouponsRemind**](DefaultApi.md#clientCouponsRemind) | **POST** /me/remind/coupons | Отправка уведомлений о купонах при приближении к ближайшей АЗС
[**clientInfo**](DefaultApi.md#clientInfo) | **GET** /me | Получение информации о клиенте
[**clientQrCode**](DefaultApi.md#clientQrCode) | **GET** /me/qrcode | Получение временного QR-кода клиента
[**completeAuth**](DefaultApi.md#completeAuth) | **POST** /complete_auth | Проверка одноразового пароля авторизации/регистрации
[**deviceSettingsList**](DefaultApi.md#deviceSettingsList) | **POST** /device/settings/list | Получение информации о настройках устройства
[**deviceSettingsSave**](DefaultApi.md#deviceSettingsSave) | **POST** /device/settings/save | Сохранение информации о настройках устройства
[**getContentSection**](DefaultApi.md#getContentSection) | **GET** /content/{code} | Получение раздела со статьями
[**getCoupon**](DefaultApi.md#getCoupon) | **GET** /coupons/{id} | Получение купона
[**getCoupons**](DefaultApi.md#getCoupons) | **GET** /coupons | Список общих купонов
[**getMessageSubjects**](DefaultApi.md#getMessageSubjects) | **GET** /messages/subjects | Получение списка тем сообщения
[**getNotifications**](DefaultApi.md#getNotifications) | **GET** /me/notifications | Список сообщений пользователя
[**getPlaceServices**](DefaultApi.md#getPlaceServices) | **GET** /place_services | Список услуг торговых точек
[**getPlaces**](DefaultApi.md#getPlaces) | **GET** /places | Список торговых точек
[**getProducts**](DefaultApi.md#getProducts) | **GET** /products | Список товаров (видов топлива)
[**notificationsDeleteAll**](DefaultApi.md#notificationsDeleteAll) | **DELETE** /me/notifications | Удаление всех сообщений
[**notificationsDeleteOne**](DefaultApi.md#notificationsDeleteOne) | **DELETE** /me/notifications/{id} | Удаление одного сообщения
[**notificationsReadAll**](DefaultApi.md#notificationsReadAll) | **GET** /me/notifications/read_all | Прочтение всех сообщений
[**notificationsReadOne**](DefaultApi.md#notificationsReadOne) | **GET** /me/notifications/{id}/read | Прочтение одного сообщения
[**setCouponRemind**](DefaultApi.md#setCouponRemind) | **POST** /me/remind/coupons/{id} | Установка признака уведомления клиента о купоне при приближении к заправке
[**updateClientInfo**](DefaultApi.md#updateClientInfo) | **POST** /me | Обновление информации о клиенте


# **addMessage**
> InlineResponse2007 addMessage(subjects, email, fio, description)

Добавление сообщения клиента

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final subjects = [subjects_example]; // List<String> | Код темы обращения
final email = email_example; // String | E-mail
final fio = fio_example; // String | Имя
final description = description_example; // String | Текст обращения

try { 
    final result = api_instance.addMessage(subjects, email, fio, description);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->addMessage: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **subjects** | [**List<String>**](String.md)| Код темы обращения | 
 **email** | **String**| E-mail | 
 **fio** | **String**| Имя | 
 **description** | **String**| Текст обращения | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **auth**
> InlineResponse200 auth(phoneNumber)

Запрос одноразового пароля по номеру телефона

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final phoneNumber = phoneNumber_example; // String | Номер телефона клиента в формате 79139050305

try { 
    final result = api_instance.auth(phoneNumber);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->auth: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phoneNumber** | **String**| Номер телефона клиента в формате 79139050305 | 

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **clientCoupons**
> InlineResponse2006 clientCoupons()

Список персональных купонов клиента

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();

try { 
    final result = api_instance.clientCoupons();
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->clientCoupons: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **clientCouponsRemind**
> InlineResponse2007 clientCouponsRemind(placeIds)

Отправка уведомлений о купонах при приближении к ближайшей АЗС

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final placeIds = [placeIds_example]; // List<String> | Id ближайших заправок

try { 
    final result = api_instance.clientCouponsRemind(placeIds);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->clientCouponsRemind: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **placeIds** | [**List<String>**](String.md)| Id ближайших заправок | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **clientInfo**
> InlineResponse2002 clientInfo()

Получение информации о клиенте

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();

try { 
    final result = api_instance.clientInfo();
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->clientInfo: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **clientQrCode**
> InlineResponse2003 clientQrCode()

Получение временного QR-кода клиента

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();

try { 
    final result = api_instance.clientQrCode();
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->clientQrCode: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **completeAuth**
> InlineResponse2001 completeAuth(phoneNumber, code, deviceToken)

Проверка одноразового пароля авторизации/регистрации

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final phoneNumber = phoneNumber_example; // String | Номер телефона клиента в формате 79139050305
final code = code_example; // String | Одноразовый пароль клиента из СМС
final deviceToken = deviceToken_example; // String | Device token устройства

try { 
    final result = api_instance.completeAuth(phoneNumber, code, deviceToken);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->completeAuth: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phoneNumber** | **String**| Номер телефона клиента в формате 79139050305 | 
 **code** | **String**| Одноразовый пароль клиента из СМС | 
 **deviceToken** | **String**| Device token устройства | 

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deviceSettingsList**
> InlineResponse2004 deviceSettingsList(deviceUdid, deviceToken, deviceType)

Получение информации о настройках устройства

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final deviceUdid = deviceUdid_example; // String | Udid устройства
final deviceToken = deviceToken_example; // String | Device token устройства
final deviceType = deviceType_example; // String | Тип устройства

try { 
    final result = api_instance.deviceSettingsList(deviceUdid, deviceToken, deviceType);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->deviceSettingsList: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceUdid** | **String**| Udid устройства | 
 **deviceToken** | **String**| Device token устройства | 
 **deviceType** | **String**| Тип устройства | 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deviceSettingsSave**
> InlineResponse2004 deviceSettingsSave(deviceToken, geoEnabled, notificationsEnabled, adEnabled, personalOffersEnabled, techInfoEnabled)

Сохранение информации о настройках устройства

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final deviceToken = deviceToken_example; // String | Device token устройства
final geoEnabled = 56; // int | Геолокация включена (0 либо 1)
final notificationsEnabled = 56; // int | Все уведомления включены (0 либо 1)
final adEnabled = 56; // int | Все рекламные акции включены (0 либо 1)
final personalOffersEnabled = 56; // int | Индивидуальные предложения включены (0 либо 1)
final techInfoEnabled = 56; // int | Техническая информация включена (0 либо 1)

try { 
    final result = api_instance.deviceSettingsSave(deviceToken, geoEnabled, notificationsEnabled, adEnabled, personalOffersEnabled, techInfoEnabled);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->deviceSettingsSave: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceToken** | **String**| Device token устройства | 
 **geoEnabled** | **int**| Геолокация включена (0 либо 1) | [optional] 
 **notificationsEnabled** | **int**| Все уведомления включены (0 либо 1) | [optional] 
 **adEnabled** | **int**| Все рекламные акции включены (0 либо 1) | [optional] 
 **personalOffersEnabled** | **int**| Индивидуальные предложения включены (0 либо 1) | [optional] 
 **techInfoEnabled** | **int**| Техническая информация включена (0 либо 1) | [optional] 

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getContentSection**
> InlineResponse20013 getContentSection(code, page, perPage)

Получение раздела со статьями

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final code = code_example; // String | Код раздела
final page = 56; // int | Пагинация для статей, входящих в раздел
final perPage = 56; // int | Статей раздела на страницу

try { 
    final result = api_instance.getContentSection(code, page, perPage);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getContentSection: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **code** | **String**| Код раздела | 
 **page** | **int**| Пагинация для статей, входящих в раздел | [optional] 
 **perPage** | **int**| Статей раздела на страницу | [optional] 

### Return type

[**InlineResponse20013**](InlineResponse20013.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getCoupon**
> InlineResponse2009 getCoupon(id)

Получение купона

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final id = 56; // int | Id купона

try { 
    final result = api_instance.getCoupon(id);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getCoupon: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id купона | 

### Return type

[**InlineResponse2009**](InlineResponse2009.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getCoupons**
> InlineResponse2005 getCoupons(page, perPage)

Список общих купонов

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final page = 56; // int | Пагинация
final perPage = 56; // int | Записей на страницу

try { 
    final result = api_instance.getCoupons(page, perPage);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getCoupons: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **int**| Пагинация | [optional] 
 **perPage** | **int**| Записей на страницу | [optional] 

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getMessageSubjects**
> InlineResponse20014 getMessageSubjects()

Получение списка тем сообщения

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();

try { 
    final result = api_instance.getMessageSubjects();
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getMessageSubjects: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20014**](InlineResponse20014.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getNotifications**
> InlineResponse2008 getNotifications(deviceToken, page, perPage, search)

Список сообщений пользователя

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final deviceToken = deviceToken_example; // String | Token устройства
final page = 56; // int | Пагинация для сообщений
final perPage = 56; // int | Сообщений на страницу
final search = search_example; // String | Поисковый запрос

try { 
    final result = api_instance.getNotifications(deviceToken, page, perPage, search);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getNotifications: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceToken** | **String**| Token устройства | 
 **page** | **int**| Пагинация для сообщений | [optional] 
 **perPage** | **int**| Сообщений на страницу | [optional] 
 **search** | **String**| Поисковый запрос | [optional] 

### Return type

[**InlineResponse2008**](InlineResponse2008.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPlaceServices**
> InlineResponse20011 getPlaceServices()

Список услуг торговых точек

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();

try { 
    final result = api_instance.getPlaceServices();
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getPlaceServices: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20011**](InlineResponse20011.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getPlaces**
> InlineResponse20010 getPlaces(productIds, serviceIds, search)

Список торговых точек

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();
final productIds = []; // List<String> | Фильтр торговых точек по товарам (топливу)
final serviceIds = []; // List<String> | Фильтр торговых точек по видам услуг
final search = search_example; // String | Поисковый запрос

try { 
    final result = api_instance.getPlaces(productIds, serviceIds, search);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getPlaces: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productIds** | [**List<String>**](String.md)| Фильтр торговых точек по товарам (топливу) | [optional] [default to const []]
 **serviceIds** | [**List<String>**](String.md)| Фильтр торговых точек по видам услуг | [optional] [default to const []]
 **search** | **String**| Поисковый запрос | [optional] 

### Return type

[**InlineResponse20010**](InlineResponse20010.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **getProducts**
> InlineResponse20012 getProducts()

Список товаров (видов топлива)

### Example 
```dart
import 'package:prognoz_api/api.dart';

final api_instance = DefaultApi();

try { 
    final result = api_instance.getProducts();
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->getProducts: $e\n');
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**InlineResponse20012**](InlineResponse20012.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notificationsDeleteAll**
> InlineResponse2007 notificationsDeleteAll(deviceToken, page, perPage, search)

Удаление всех сообщений

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final deviceToken = deviceToken_example; // String | Token устройства
final page = 56; // int | Пагинация для сообщений
final perPage = 56; // int | Сообщений на страницу
final search = search_example; // String | Поисковый запрос

try { 
    final result = api_instance.notificationsDeleteAll(deviceToken, page, perPage, search);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->notificationsDeleteAll: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceToken** | **String**| Token устройства | 
 **page** | **int**| Пагинация для сообщений | [optional] 
 **perPage** | **int**| Сообщений на страницу | [optional] 
 **search** | **String**| Поисковый запрос | [optional] 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notificationsDeleteOne**
> InlineResponse2007 notificationsDeleteOne(id, deviceToken)

Удаление одного сообщения

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | Id сообщения
final deviceToken = deviceToken_example; // String | Token устройства

try { 
    final result = api_instance.notificationsDeleteOne(id, deviceToken);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->notificationsDeleteOne: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)| Id сообщения | 
 **deviceToken** | **String**| Token устройства | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notificationsReadAll**
> InlineResponse2007 notificationsReadAll(deviceToken)

Прочтение всех сообщений

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final deviceToken = deviceToken_example; // String | Token устройства

try { 
    final result = api_instance.notificationsReadAll(deviceToken);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->notificationsReadAll: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **deviceToken** | **String**| Token устройства | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **notificationsReadOne**
> InlineResponse2007 notificationsReadOne(id, deviceToken)

Прочтение одного сообщения

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final id = 38400000-8cf0-11bd-b23e-10b96e4ef00d; // String | Id сообщения
final deviceToken = deviceToken_example; // String | Token устройства

try { 
    final result = api_instance.notificationsReadOne(id, deviceToken);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->notificationsReadOne: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | [**String**](.md)| Id сообщения | 
 **deviceToken** | **String**| Token устройства | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **setCouponRemind**
> InlineResponse2007 setCouponRemind(id, remind)

Установка признака уведомления клиента о купоне при приближении к заправке

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final id = 56; // int | Id купона
final remind = 56; // int | Признак уведомления у купона. Отправлять 0 либо 1

try { 
    final result = api_instance.setCouponRemind(id, remind);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->setCouponRemind: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Id купона | 
 **remind** | **int**| Признак уведомления у купона. Отправлять 0 либо 1 | 

### Return type

[**InlineResponse2007**](InlineResponse2007.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **updateClientInfo**
> InlineResponse2002 updateClientInfo(phoneNumber, email, city, name, surname, favoriteFuelId, VK, facebook)

Обновление информации о клиенте

### Example 
```dart
import 'package:prognoz_api/api.dart';
// TODO Configure API key authorization: JWT
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKey = 'YOUR_API_KEY';
// uncomment below to setup prefix (e.g. Bearer) for API key, if needed
//defaultApiClient.getAuthentication<ApiKeyAuth>('JWT').apiKeyPrefix = 'Bearer';

final api_instance = DefaultApi();
final phoneNumber = phoneNumber_example; // String | Номер телефона
final email = email_example; // String | Email
final city = city_example; // String | Город
final name = name_example; // String | Имя
final surname = surname_example; // String | Фамилия
final favoriteFuelId = favoriteFuelId_example; // String | Id любимого вида топлива
final VK = VK_example; // String | ВКонтакте клиента
final facebook = facebook_example; // String | Facebook клиента

try { 
    final result = api_instance.updateClientInfo(phoneNumber, email, city, name, surname, favoriteFuelId, VK, facebook);
    print(result);
} catch (e) {
    print('Exception when calling DefaultApi->updateClientInfo: $e\n');
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **phoneNumber** | **String**| Номер телефона | [optional] 
 **email** | **String**| Email | [optional] 
 **city** | **String**| Город | [optional] 
 **name** | **String**| Имя | [optional] 
 **surname** | **String**| Фамилия | [optional] 
 **favoriteFuelId** | **String**| Id любимого вида топлива | [optional] 
 **VK** | **String**| ВКонтакте клиента | [optional] 
 **facebook** | **String**| Facebook клиента | [optional] 

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

[JWT](../README.md#JWT)

### HTTP request headers

 - **Content-Type**: multipart/form-data
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

