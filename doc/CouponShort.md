# prognoz_api.model.CouponShort

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**name** | **String** | Название купона | [optional] 
**finishedAt** | **String** | Дата окончания в формате 2020-11-08T00:00:00 | [optional] 
**finishedAtLabel** | **String** | Надпись даты окончания в формате до 25 января 2020 | [optional] 
**imageUrl** | **String** | Ссылка на файл изображения купона | [optional] 
**placeIds** | **List<String>** | Id торговых точек | [optional] [default to const []]
**remind** | **bool** | Признак уведомлять ли о купоне при приближении к АЗС | [optional] 
**productId** | **String** | Id товара | [optional] 
**productExternalId** | **String** | Номенклатурный номер товара | [optional] 
**productName** | **String** | Название товара | [optional] 
**price** | **String** | Цена товара по купону | [optional] 
**productTypeId** | **int** | Id типа товара. Если равно 1, то это цена любимого топлива клиента | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


