# prognoz_api.model.ContentSubSection

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id подраздела | [optional] 
**code** | **String** | Код подраздела | [optional] 
**title** | **String** | Заголовок статьи | [optional] 
**description** | **String** | Текст статьи | [optional] 
**imageUrl** | **String** | Ссылка на изображение статьи | [optional] 
**sort** | **int** | Индекс сортировки | [optional] 
**buttons** | **List<String>** |  | [optional] [default to const []]
**contentButtons** | [**List<ContentButton>**](ContentButton.md) |  | [optional] [default to const []]
**children** | [**List<ContentArticle>**](ContentArticle.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


