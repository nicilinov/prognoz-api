# prognoz_api.model.ClientStatus

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Код статуса клиента | [optional] 
**name** | **String** | Название статуса клиента | [optional] 
**remindedAt** | **String** | Дата и время последнего уведомления о купонах при приближении к АЗС | [optional] 
**hasCouponReminds** | **bool** | Признак, что у клиента есть актуальные купоны с уведомлениями | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


