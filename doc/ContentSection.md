# prognoz_api.model.ContentSection

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id раздела | [optional] 
**code** | **String** | Код раздела | [optional] 
**title** | **String** | Заголовок статьи | [optional] 
**description** | **String** | Текст статьи | [optional] 
**imageUrl** | **String** | Ссылка на изображение статьи | [optional] 
**sort** | **int** | Индекс сортировки | [optional] 
**children** | [**List<ContentSubSection>**](ContentSubSection.md) |  | [optional] [default to const []]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


