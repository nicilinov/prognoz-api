# prognoz_api.model.DeviceSettings

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**geoEnabled** | **bool** | Геолокация включена | [optional] 
**notificationsEnabled** | **bool** | Все уведомления включены | [optional] 
**adEnabled** | **bool** | Все рекламные акции включены | [optional] 
**personalOffersEnabled** | **bool** | Индивидуальные предложения включены | [optional] 
**techInfoEnabled** | **bool** | Техническая информация включена | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


