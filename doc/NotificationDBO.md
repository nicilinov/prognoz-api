# prognoz_api.model.NotificationDBO

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | Id сообщения | [optional] 
**title** | **String** | Заголовок сообщения | [optional] 
**message** | **String** | Текст сообщения | [optional] 
**type** | [**NotificationType**](NotificationType.md) |  | [optional] 
**sentAt** | **String** | Дата и время отправки сообщения в формате 2020-11-08T00:00:00 | [optional] 
**read** | **bool** | Признак прочтения | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


