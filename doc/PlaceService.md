# prognoz_api.model.PlaceService

## Load the model package
```dart
import 'package:prognoz_api/api.dart';
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | Id услуги | [optional] 
**name** | **String** | Название услуги | [optional] 
**iconUrl** | **String** | Ссылка на файл иконки услуги | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


