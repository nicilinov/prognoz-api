import 'package:prognoz_api/api.dart';
import 'package:test/test.dart';

// tests for Product
void main() {
  final instance = Product();

  group('test Product', () {
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // Название товара
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // ProductType type
    test('to test the property `type`', () async {
      // TODO
    });

    // Признак участия в бонусной программе
    // bool isBonus
    test('to test the property `isBonus`', () async {
      // TODO
    });

    // Коэффициенты накопления баллов
    // List<num> bonusAdd (default value: const [])
    test('to test the property `bonusAdd`', () async {
      // TODO
    });

    // Коэффициенты списания баллов
    // List<num> bonusDel (default value: const [])
    test('to test the property `bonusDel`', () async {
      // TODO
    });

    // Коэффициенты накопления баллов на просмотре товара
    // String bonusAddLabel
    test('to test the property `bonusAddLabel`', () async {
      // TODO
    });

    // Коэффициенты списания баллов на просмотре товара
    // String bonusDelLabel
    test('to test the property `bonusDelLabel`', () async {
      // TODO
    });


  });

}
