import 'package:prognoz_api/api.dart';
import 'package:test/test.dart';

// tests for DeviceSettings
void main() {
  final instance = DeviceSettings();

  group('test DeviceSettings', () {
    // Геолокация включена
    // bool geoEnabled
    test('to test the property `geoEnabled`', () async {
      // TODO
    });

    // Все уведомления включены
    // bool notificationsEnabled
    test('to test the property `notificationsEnabled`', () async {
      // TODO
    });

    // Все рекламные акции включены
    // bool adEnabled
    test('to test the property `adEnabled`', () async {
      // TODO
    });

    // Индивидуальные предложения включены
    // bool personalOffersEnabled
    test('to test the property `personalOffersEnabled`', () async {
      // TODO
    });

    // Техническая информация включена
    // bool techInfoEnabled
    test('to test the property `techInfoEnabled`', () async {
      // TODO
    });


  });

}
