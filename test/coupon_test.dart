import 'package:prognoz_api/api.dart';
import 'package:test/test.dart';

// tests for Coupon
void main() {
  final instance = Coupon();

  group('test Coupon', () {
    // int id
    test('to test the property `id`', () async {
      // TODO
    });

    // Название купона
    // String name
    test('to test the property `name`', () async {
      // TODO
    });

    // Описание купона
    // String description
    test('to test the property `description`', () async {
      // TODO
    });

    // Дата окончания в формате 2020-11-08T00:00:00
    // String finishedAt
    test('to test the property `finishedAt`', () async {
      // TODO
    });

    // Надпись даты окончания в формате до 25 января 2020
    // String finishedAtLabel
    test('to test the property `finishedAtLabel`', () async {
      // TODO
    });

    // Id торговых точек
    // List<String> placeIds (default value: const [])
    test('to test the property `placeIds`', () async {
      // TODO
    });

    // Ссылка на файл изображения купона
    // String imageUrl
    test('to test the property `imageUrl`', () async {
      // TODO
    });

    // List<String> galleryUrls (default value: const [])
    test('to test the property `galleryUrls`', () async {
      // TODO
    });

    // код купона из 10 цифр
    // String number
    test('to test the property `number`', () async {
      // TODO
    });

    // Шрих-код купона в формате изображения в base64
    // String barcode
    test('to test the property `barcode`', () async {
      // TODO
    });

    // Признак уведомлять ли о купоне при приближении к АЗС
    // bool remind
    test('to test the property `remind`', () async {
      // TODO
    });

    // Id товара
    // String productId
    test('to test the property `productId`', () async {
      // TODO
    });

    // Номенклатурный номер товара
    // String productExternalId
    test('to test the property `productExternalId`', () async {
      // TODO
    });

    // Название товара
    // String productName
    test('to test the property `productName`', () async {
      // TODO
    });

    // Цена товара по купону
    // String price
    test('to test the property `price`', () async {
      // TODO
    });

    // Id типа товара. Если равно 1, то это цена любимого топлива клиента
    // int productTypeId
    test('to test the property `productTypeId`', () async {
      // TODO
    });


  });

}
