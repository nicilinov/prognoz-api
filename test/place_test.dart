import 'package:prognoz_api/api.dart';
import 'package:test/test.dart';

// tests for Place
void main() {
  final instance = Place();

  group('test Place', () {
    // Id торговой точки
    // String id
    test('to test the property `id`', () async {
      // TODO
    });

    // Название торговой точки
    // String title
    test('to test the property `title`', () async {
      // TODO
    });

    // Адрес торговой точки
    // String address
    test('to test the property `address`', () async {
      // TODO
    });

    // Широта торговой точки
    // num latitude
    test('to test the property `latitude`', () async {
      // TODO
    });

    // Долгота торговой точки
    // num longtitude
    test('to test the property `longtitude`', () async {
      // TODO
    });

    // List<PlaceService> services (default value: const [])
    test('to test the property `services`', () async {
      // TODO
    });


  });

}
