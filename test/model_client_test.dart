import 'package:prognoz_api/api.dart';
import 'package:test/test.dart';

// tests for ModelClient
void main() {
  final instance = ModelClient();

  group('test ModelClient', () {
    // Номер карты лояльности клиента
    // String cardId
    test('to test the property `cardId`', () async {
      // TODO
    });

    // Номер телефона клиента
    // String phoneNumber
    test('to test the property `phoneNumber`', () async {
      // TODO
    });

    // E-mail клиента
    // String email
    test('to test the property `email`', () async {
      // TODO
    });

    // E-mail клиента подтверждён?
    // bool isEmailConfirmed
    test('to test the property `isEmailConfirmed`', () async {
      // TODO
    });

    // Город клиента
    // String city
    test('to test the property `city`', () async {
      // TODO
    });

    // ClientStatus status
    test('to test the property `status`', () async {
      // TODO
    });

    // Ф.И.О. клиента
    // String fio
    test('to test the property `fio`', () async {
      // TODO
    });

    // Баланс клиента
    // int balance
    test('to test the property `balance`', () async {
      // TODO
    });

    // Любимый вид топлива клиента
    // String favoriteFuelName
    test('to test the property `favoriteFuelName`', () async {
      // TODO
    });

    // Дата регистрации
    // String createdAt
    test('to test the property `createdAt`', () async {
      // TODO
    });

    // ClientContacts contacts
    test('to test the property `contacts`', () async {
      // TODO
    });

    // Геолокация включена
    // bool geoEnabled
    test('to test the property `geoEnabled`', () async {
      // TODO
    });

    // Все уведомления включены
    // bool notificationsEnabled
    test('to test the property `notificationsEnabled`', () async {
      // TODO
    });

    // Все рекламные акции включены
    // bool adEnabled
    test('to test the property `adEnabled`', () async {
      // TODO
    });

    // Индивидуальные предложения включены
    // bool personalOffersEnabled
    test('to test the property `personalOffersEnabled`', () async {
      // TODO
    });

    // Техническая информация включена
    // bool techInfoEnabled
    test('to test the property `techInfoEnabled`', () async {
      // TODO
    });


  });

}
